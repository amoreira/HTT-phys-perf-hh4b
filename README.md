# HTT physics peformance studies

This project contains the code to perform physics performance studies in the hh->4b channel. The main goal is to understand how bad can the b-tagging at the trigger be such that it does not affect the offline analysis. To do so, we need to parameterize the b-tagging performance at the trigger. 

Since the HTT is not taking data yet and the full simulation is still in its early stages we do not have a precise b-tagging parameterization for the HTT. Nonetheless, we can estimate its performance. We start from the offline b-tagging parameterization for the upgrade and then worsen the b-tagging (and light/c mistags) efficiencies in order to emulate the performance at the trigger. To do so we draw inspiration from FTK even though the studies on b-tagging performance are still rather preliminary.

The code uses the EventLoop framework, based on AnalysisBase (see tutorial here https://atlassoftwaredocs.web.cern.ch/ABtutorial/).

In this branch, the code is divided in two packages:

- **MyxAODAnalysis**: it takes xAOD samples and applies trigger pre-selection corresponding to the trigger chains implemented
- **MyxAODAnalysisBTagging**: it takes the skimmed xAOD samples (produced by MyxAODAnalysis), applies b-tagging parameterizations and performs offline analysis

## MyxAODAnalysis

**Brief description of code**

For each of the trigger chains being considered we apply pre-selections corresponding to the L1 and HLT selections. We apply every cut except the ones depending on b-tagging. For each trigger chain we add a boolean variable that indicates whether or not the event passed the trigger pre-selection.

We keep events that pass at least one of the pre-selections. For each event we keep only the EventInfo and AntiKt4EMPflowJets collections.

The code is supposed to be ran on the grid only once (or as few times as possible). Running locally is also possible but it will take too long due to the size and complexity of the samples.

**Input**: full sim xAOD samples for upgrade (14 TeV, pileup 200)

**Output**:

1. simplified xAOD samples with AntiKt4EMPFlowJets and EventInfo collections and with information of what trigger pre-selection was passed
2. root file with cutflow histograms for each trigger chain

## MyxAODAnalysisBTagging

**Brief description of code**

Taking as input the xAOD samples produced by the MyxAODAnalysis code, it applies a b-tagging parameterization and selects the events that pass the b-tagging requirements of the trigger chains. In addition, for the events passing at least one of the trigger chains it emulates the offline analysis. 

At the moment (July 2019) two b-tagging parameterization points are implemented:

- Offline: implemented using the UpgradePerformanceFunctions (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/UpgradePerformanceFunctions)
- FTK-like: estimated (and confirmed) using the following plots. From the first plot we take the FTK b-tagging efficiency w.r.t. offline. SInce we have the offline b-tagging efficiency we can then multiply both numbers and obtain an overall FTK-like b-tagging efficiency. The second plot, while very preliminary, allows us to confirm that the mistag rates make sense.

![Semantic description of image](images/FTK.png)

![Semantic description of image](images/FTK_btagging.png)

The offline analysis follows http://cds.cern.ch/record/2313703. The cuts are the following:

- At least 4j40
- At least one Higgs pair that fullfils Dhh requirement
- pT(leading Higgs) > 0.5 m4j - 103
- pT(subleading Higgs) > 0.33 m4j - 73
- |delta eta (hh)| < 1.5
- Xhh < 1.6
- Xwt > 1.5

**Input**: output xAOD from MyxAODAnalysis

**Output**: root file with histograms of kinematic distributions and cutflows

# What files to change and what do they do

## Source file

This is your main code where you process the data samples and choose what to do with them (accessing collections of objects, fill histograms, ...). This is the file you need to change if you want to add new features, histograms, functionalities to the code.

**Located under:** HTT-phys-perf-hh4b/source/MyAnalysisBTagging/Root/MyxAODAnalysisBTagging.cxx

## Configuration file

This file contains the instructions to run the code. For example, which sample and how many events to run over, if you want to submit it locally, to a batch system or the grid.

To change the sample to use you need to change the `inputFilePath` and the `inputFilePattern` in the following lines: 

`inputFilePath = '/eos/user/a/amoreira/user.amoreira.JZ0_r9871_presel_grid_030619_ANALYSIS.root'`

`inputFilePattern = 'user*'`

`ROOT.SH.ScanDir().filePattern( inputFilePattern ).scan( sh, inputFilePath )`

To change the number of events to run over you need to change the line:

`job.options().setDouble( ROOT.EL.Job.optMaxEvents, 10000 )`

# Getting and running the code

## First time

**Get the repository:**
- create you own branch of my repository: https://gitlab.cern.ch/amoreira/HTT-phys-perf-hh4b -> new branch -> give name of your choice -> create form: lxplus_grid_segment_code 
- login to fermi
- `setupATLAS`
- `lsetup git`
- `git clone ssh://git@gitlab.cern.ch:7999/amoreira/HTT-phys-perf-hh4b.git` (will create directory named HTT-phys-perf-hh4b)
- `cd HTT-phys-perf-hh4b`
- `git checkout *name of your branch*`

**Compile and run:**
- `cd HTT-phys-perf-hh4b`
- `mkdir build` (place where you compile the code)
- `mkdir run` (place where you run the code)
- `cd build`
- `asetup AnalysisBase,21.2.68` (setting up analysis release)
- `cmake ../source` (compile code)
- `make` (compile code)
- `source */setup.sh`
- `cd run`
- `ATestRun.py --submission-dir = *name of directory*` (actually running the code)

## Following times

**Compile and run:**

- `cd HTT-phys-perf-hh4b`
- `cd build`
- `asetup --restore` (restore previously set up analysis release)
- `cmake ../source` (compile code)
- `make` (compile code)
- `source */setup.sh`
- `cd run`
- `ATestRun.py --submission-dir = *name of directory*`

## Submitting jobs to the fermi machines at LIP

From inside a fermi machine:

- submit script located under: /home/t3atlas/aluisa/subHTT.sh
- output goes (must go) to lstore
- `qsub subHTT.sh` (to submit job)
- `qstat` (to check status of the job)
