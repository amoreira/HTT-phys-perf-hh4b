
void plot_hhSampleStudy(){
  
  string path = "/afs/cern.ch/work/a/amoreira/";
  gROOT->LoadMacro((path+"AtlasStyle.h").c_str());
  gROOT->LoadMacro((path+"AtlasStyle.C").c_str());
  gROOT->ProcessLine("SetAtlasStyle()");
  gROOT->LoadMacro((path+"AtlasLabels.C").c_str());

  // Open files
  //TFile *f10 = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/submitDir_pt10/hist-mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871.root");
  TFile *f20 = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/submitDir_pt20/hist-mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871.root");

  // Get histograms
  // pt dif between b quarks and reco jets
  //TH1F *hist_ptdif_pt10 = (TH1F*)f10->Get("hist_ptdif_truth_EMPFlow_bquark");
  TH1F *hist_ptdif_pt20 = (TH1F*)f20->Get("hist_ptdif_truth_EMPFlow_bquark");
  hist_ptdif_pt20->SetLineColor(kRed);
  hist_ptdif_pt20->Rebin(4);
  // hist_ptdif_pt10->Rebin(4);
  // profile
  //TH1F *hist_profile_pt10 = (TH1F*)f10->Get("hist_profile_ptdif_pt_bquark");
  TH1F *hist_profile_pt20 = (TH1F*)f20->Get("hist_profile_ptdif_pt_bquark");
  hist_profile_pt20->SetLineColor(kRed);
  hist_profile_pt20->Rebin(4);
  // hist_profile_pt10->Rebin(4);
  // pt reco no cut on b quarks
  //TH1F *hist_ptreco_pt10 = (TH1F*)f10->Get("hist_ptreco_matchHiggs");
  TH1F *hist_ptreco_pt20 = (TH1F*)f20->Get("hist_ptreco_matchHiggs");
  // pt reco pt b quarks > 20
  // TH1F *hist_ptreco_ptbquark20_pt10 = (TH1F*)f10->Get("hist_ptreco_matchHiggs_pt20");
  TH1F *hist_ptreco_ptbquark20_pt20 = (TH1F*)f20->Get("hist_ptreco_matchHiggs_pt20");
  // pt reco pt b quarks > 25
  //TH1F *hist_ptreco_ptbquark25_pt10 = (TH1F*)f10->Get("hist_ptreco_matchHiggs_pt25");
  TH1F *hist_ptreco_ptbquark25_pt20 = (TH1F*)f20->Get("hist_ptreco_matchHiggs_pt25");

  TCanvas *c1 = new TCanvas();
  //hist_ptdif_pt10->Draw();
  hist_ptdif_pt20->Draw("e");
  hist_ptdif_pt20->GetXaxis()->SetTitle("(p_{T}^{reco}-p_{T}^{truth})/p_{T}^{truth}");
  hist_ptdif_pt20->GetYaxis()->SetTitle("No. of jets");
  c1->SaveAs("ptdif.pdf");

  TCanvas *c2 = new TCanvas();
  //hist_profile_pt10->Draw();
  hist_profile_pt20->Draw("e");
  hist_profile_pt20->GetYaxis()->SetTitle("(p_{T}^{reco}-p_{T}^{truth})/p_{T}^{truth}");
  hist_profile_pt20->GetXaxis()->SetTitle("p_{T}^{truth} [GeV]");
  c2->SaveAs("profile.pdf");

  TCanvas *c3 = new TCanvas();
  hist_ptreco_pt20->SetLineColor(kBlue);
  hist_ptreco_pt20->Draw();
  hist_ptreco_pt20->GetXaxis()->SetRangeUser(0,100);
  hist_ptreco_ptbquark20_pt20->SetLineColor(kRed);
  hist_ptreco_ptbquark20_pt20->GetXaxis()->SetRangeUser(0,100);
  hist_ptreco_ptbquark20_pt20->Draw("same");
  hist_ptreco_pt20->GetXaxis()->SetTitle("p_{T}^{reco} [GeV]");
  hist_ptreco_pt20->GetYaxis()->SetTitle("No. of jets");
  int n = 8;
  cout<<hist_ptreco_ptbquark20_pt20->Integral(1,n)<<";"<<hist_ptreco_pt20->Integral(1,n)<<endl;
  cout<<TMath::Abs(hist_ptreco_ptbquark20_pt20->Integral(1,n)-hist_ptreco_pt20->Integral(1,n))/hist_ptreco_pt20->Integral(1,n)<<endl;

  TLegend *leg = new TLegend(0.15,0.7,0.65,0.9);
  leg->AddEntry(hist_ptreco_pt20,"Jets matched to b quarks","f");
  leg->AddEntry(hist_ptreco_ptbquark20_pt20,"Jet matched to b quarks w/ p_{T}>20 GeV","f");
  leg->SetFillStyle(0);
  leg->SetLineWidth(0);
  leg->SetBorderSize(0);
  leg->Draw();

  c3->SaveAs("ptreco_pt20.pdf");
  
  /*  TCanvas *c4 = new TCanvas();
  hist_ptreco_pt20->Draw("e");
  hist_ptreco_pt20->GetXaxis()->SetRangeUser(0,100);
  hist_ptreco_ptbquark25_pt20->SetLineColor(kRed);
  hist_ptreco_ptbquark25_pt20->GetXaxis()->SetRangeUser(0,100);
  hist_ptreco_ptbquark25_pt20->Draw("same");
  hist_ptreco_pt20->GetXaxis()->SetTitle("p_{T}^{reco} [GeV]");
  hist_ptreco_pt20->GetYaxis()->SetTitle("No. of jets");
  cout<<hist_ptreco_ptbquark25_pt20->Integral()<<";"<<hist_ptreco_pt20->Integral()<<endl;
  cout<<TMath::Abs(hist_ptreco_ptbquark25_pt20->Integral()-hist_ptreco_pt20->Integral())/hist_ptreco_pt20->Integral()<<endl;

  c4->SaveAs("ptreco_pt25.pdf");*/
  /*  TCanvas *c4 = new TCanvas();
  hist_ptreco_pt10->Draw();
  hist_ptreco_pt10->GetXaxis()->SetRangeUser(0,100);
  hist_ptreco_ptbquark20_pt10->SetLineColor(kRed);
  hist_ptreco_ptbquark20_pt10->GetXaxis()->SetRangeUser(0,100);
  hist_ptreco_ptbquark20_pt10->Draw("same");
  cout<<TMath::Abs(hist_ptreco_ptbquark20_pt10->Integral()-hist_ptreco_pt10->Integral())/hist_ptreco_pt10->Integral()<<endl;
  */
}
