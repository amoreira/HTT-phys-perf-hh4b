void plot_jets(){

  gStyle->SetOptStat("neou");

  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.h");
  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.C");
  gROOT->ProcessLine("SetAtlasStyle()");

  // Open file
  TFile *f = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/submitDir/hist-mc15_14TeV.301497.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M1200.recon.AOD.e6011_s3185_s3186_r9871.root");

  // Get histos for jets
  // AntiKt4TruthJets before any cuts
  // pt
  TH1F *hist_antikt4truthjet_pt = (TH1F*)f->Get("hist_antikt4truthjet_pt");
  // eta
  TH1F *hist_antikt4truthjet_eta = (TH1F*)f->Get("hist_antikt4truthjet_eta");
  // phi
  TH1F *hist_antikt4truthjet_phi = (TH1F*)f->Get("hist_antikt4truthjet_phi");
  // N
  TH1F *hist_antikt4truthjet_N = (TH1F*)f->Get("hist_antikt4truthjet_N");
  // AntiKt4TruthJets after pt>20 |eta|<4.5 and btag
  // pt
  TH1F *hist_antikt4truthjet_pt_ptcut20_etacut45_btag = (TH1F*)f->Get("hist_antikt4truthjet_pt_ptcut20_etacut45_btag");
  // eta
  TH1F *hist_antikt4truthjet_eta_ptcut20_etacut45_btag = (TH1F*)f->Get("hist_antikt4truthjet_eta_ptcut20_etacut45_btag");
  // N
  TH1F *hist_antikt4truthjet_N_ptcut20_etacut45_btag = (TH1F*)f->Get("hist_antikt4truthjet_N_ptcut20_etacut45_btag");

  TCanvas *c1 = new TCanvas();
  hist_antikt4truthjet_pt->Draw("hist");
  c1->SetLogy();
  hist_antikt4truthjet_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_antikt4truthjet_pt->GetYaxis()->SetTitle("#jets");
  c1->SaveAs("./Jets/hist_antikt4truthjet_pt.pdf");

  TCanvas *c2 = new TCanvas();
  hist_antikt4truthjet_eta->Draw("hist");
  hist_antikt4truthjet_eta->GetXaxis()->SetTitle("#eta");
  hist_antikt4truthjet_eta->GetYaxis()->SetTitle("#jets");
  c2->SaveAs("./Jets/hist_antikt4truthjet_eta.pdf");

  TCanvas *c3 = new TCanvas();
  hist_antikt4truthjet_phi->Draw("hist");
  hist_antikt4truthjet_phi->GetXaxis()->SetTitle("#phi");
  hist_antikt4truthjet_phi->GetYaxis()->SetTitle("#jets");
  c3->SaveAs("./Jets/hist_antikt4truthjet_phi.pdf");

  TCanvas *c4 = new TCanvas();
  hist_antikt4truthjet_N->Draw("hist");
  hist_antikt4truthjet_N->GetXaxis()->SetTitle("#jets/event");
  hist_antikt4truthjet_N->GetYaxis()->SetTitle("#events");
  c4->SaveAs("./Jets/hist_antikt4truthjet_N.pdf");

  TCanvas *c5 = new TCanvas();
  hist_antikt4truthjet_pt_ptcut20_etacut45_btag->Draw("hist");
  c1->SetLogy();
  hist_antikt4truthjet_pt_ptcut20_etacut45_btag->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_antikt4truthjet_pt_ptcut20_etacut45_btag->GetYaxis()->SetTitle("#jets");
  c5->SaveAs("./Jets/hist_antikt4truthjet_pt_ptcut20_etacut45_btag.pdf");

  TCanvas *c6 = new TCanvas();
  hist_antikt4truthjet_eta_ptcut20_etacut45_btag->Draw("hist");
  hist_antikt4truthjet_eta_ptcut20_etacut45_btag->GetXaxis()->SetTitle("#eta");
  hist_antikt4truthjet_eta_ptcut20_etacut45_btag->GetYaxis()->SetTitle("#jets");
  c6->SaveAs("./Jets/hist_antikt4truthjet_eta_ptcut20_etacut45_btag.pdf");

  TCanvas *c7 = new TCanvas();
  hist_antikt4truthjet_N_ptcut20_etacut45_btag->Draw("hist");
  hist_antikt4truthjet_N_ptcut20_etacut45_btag->GetXaxis()->SetTitle("#jets/event");
  hist_antikt4truthjet_N_ptcut20_etacut45_btag->GetYaxis()->SetTitle("#events");
  c7->SaveAs("./Jets/hist_antikt4truthjet_N_ptcut20_etacut45_btag.pdf");

  // AntiKt4EMPFJets before any cuts
  // pt
  TH1F *hist_antikt4EMPFjet_pt = (TH1F*)f->Get("hist_antikt4EMPFjet_pt");
  // eta
  TH1F *hist_antikt4EMPFjet_eta = (TH1F*)f->Get("hist_antikt4EMPFjet_eta");
  // phi
  TH1F *hist_antikt4EMPFjet_phi = (TH1F*)f->Get("hist_antikt4EMPFjet_phi");
  // N
  TH1F *hist_antikt4EMPFjet_N = (TH1F*)f->Get("hist_antikt4EMPFjet_N");
  // AntiKt4EMPFJets after pt>20 |eta|<4.5 and btag
  // pt
  TH1F *hist_antikt4EMPFjet_pt_ptcut20_etacut45_btag = (TH1F*)f->Get("hist_antikt4EMPFjet_pt_ptcut20_etacut45_btag");
  // eta
  TH1F *hist_antikt4EMPFjet_eta_ptcut20_etacut45_btag = (TH1F*)f->Get("hist_antikt4EMPFjet_eta_ptcut20_etacut45_btag");
  // N
  TH1F *hist_antikt4EMPFjet_N_ptcut20_etacut45_btag = (TH1F*)f->Get("hist_antikt4EMPFjet_N_ptcut20_etacut45_btag");

  TCanvas *c8 = new TCanvas();
  hist_antikt4EMPFjet_pt->Draw("hist");
  c8->SetLogy();
  hist_antikt4EMPFjet_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_antikt4EMPFjet_pt->GetYaxis()->SetTitle("#jets");
  c8->SaveAs("./Jets/hist_antikt4EMPFjet_pt.pdf");

  TCanvas *c9 = new TCanvas();
  hist_antikt4EMPFjet_eta->Draw("hist");
  hist_antikt4EMPFjet_eta->GetXaxis()->SetTitle("#eta");
  hist_antikt4EMPFjet_eta->GetYaxis()->SetTitle("#jets");
  c9->SaveAs("./Jets/hist_antikt4EMPFjet_eta.pdf");

  TCanvas *c10 = new TCanvas();
  hist_antikt4EMPFjet_phi->Draw("hist");
  hist_antikt4EMPFjet_phi->GetXaxis()->SetTitle("#phi");
  hist_antikt4EMPFjet_phi->GetYaxis()->SetTitle("#jets");
  c10->SaveAs("./Jets/hist_antikt4EMPFjet_phi.pdf");

  TCanvas *c11 = new TCanvas();
  hist_antikt4EMPFjet_N->Draw("hist");
  hist_antikt4EMPFjet_N->GetXaxis()->SetTitle("#jets/event");
  hist_antikt4EMPFjet_N->GetYaxis()->SetTitle("#events");
  c11->SaveAs("./Jets/hist_antikt4EMPFjet_N.pdf");

  TCanvas *c12 = new TCanvas();
  hist_antikt4EMPFjet_pt_ptcut20_etacut45_btag->Draw("hist");
  c1->SetLogy();
  hist_antikt4EMPFjet_pt_ptcut20_etacut45_btag->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_antikt4EMPFjet_pt_ptcut20_etacut45_btag->GetYaxis()->SetTitle("#jets");
  c12->SaveAs("./Jets/hist_antikt4EMPFjet_pt_ptcut20_etacut45_btag.pdf");

  TCanvas *c13 = new TCanvas();
  hist_antikt4EMPFjet_eta_ptcut20_etacut45_btag->Draw("hist");
  hist_antikt4EMPFjet_eta_ptcut20_etacut45_btag->GetXaxis()->SetTitle("#eta");
  hist_antikt4EMPFjet_eta_ptcut20_etacut45_btag->GetYaxis()->SetTitle("#jets");
  c13->SaveAs("./Jets/hist_antikt4EMPFjet_eta_ptcut20_etacut45_btag.pdf");

  TCanvas *c14 = new TCanvas();
  hist_antikt4EMPFjet_N_ptcut20_etacut45_btag->Draw("hist");
  hist_antikt4EMPFjet_N_ptcut20_etacut45_btag->GetXaxis()->SetTitle("#jets/event");
  hist_antikt4EMPFjet_N_ptcut20_etacut45_btag->GetYaxis()->SetTitle("#events");
  c14->SaveAs("./Jets/hist_antikt4EMPFjet_N_ptcut20_etacut45_btag.pdf");

  // Plots of the matching between truth b quarks and truth jets
  // pt
  TH1F *hist_truthjet_b_pt = (TH1F*)f->Get("hist_truthjet_b_pt");
  // deltaR
  TH1F *hist_truthjet_b_deltaR = (TH1F*)f->Get("hist_truthjet_b_deltaR");
  
  TCanvas *c15 = new TCanvas();
  hist_truthjet_b_pt->Draw();
  c15->SetLogy();
  hist_truthjet_b_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_truthjet_b_pt->GetYaxis()->SetTitle("#jets");
  c15->SaveAs("./Jets/hist_truthjet_b_pt.pdf");

  TCanvas *c16 = new TCanvas();
  hist_truthjet_b_deltaR->Draw();
  hist_truthjet_b_deltaR->GetXaxis()->SetTitle("#Delta R");
  hist_truthjet_b_deltaR->GetYaxis()->SetTitle("#jets");
  c16->SaveAs("./Jets/hist_truthjet_b_deltaR.pdf");

  // Plots of the matching between truth b quarks and reco jets
  // pt
  TH1F *hist_jet_b_pt = (TH1F*)f->Get("hist_jet_b_pt");
  // deltaR
  TH1F *hist_jet_b_deltaR = (TH1F*)f->Get("hist_jet_b_deltaR");
  
  TCanvas *c17 = new TCanvas();
  hist_jet_b_pt->Draw();
  c17->SetLogy();
  hist_jet_b_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_jet_b_pt->GetYaxis()->SetTitle("#jets");
  c17->SaveAs("./Jets/hist_jet_b_pt.pdf");

  TCanvas *c18 = new TCanvas();
  hist_jet_b_deltaR->Draw();
  hist_jet_b_deltaR->GetXaxis()->SetTitle("#Delta R");
  hist_jet_b_deltaR->GetYaxis()->SetTitle("#jets");
  c18->SaveAs("./Jets/hist_jet_b_deltaR.pdf");

  // Plots of the matching between truth and reco jets
  // pt
  TH1F *hist_jet_truthjet_pt = (TH1F*)f->Get("hist_jet_truthjet_pt");
  // deltaR
  TH1F *hist_jet_truthjet_deltaR = (TH1F*)f->Get("hist_jet_truthjet_deltaR");
  
  TCanvas *c19 = new TCanvas();
  hist_jet_truthjet_pt->Draw();
  c19->SetLogy();
  hist_jet_truthjet_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  hist_jet_truthjet_pt->GetYaxis()->SetTitle("#jets");
  c19->SaveAs("./Jets/hist_jet_truthjet_pt.pdf");

  TCanvas *c20 = new TCanvas();
  hist_jet_truthjet_deltaR->Draw();
  hist_jet_truthjet_deltaR->GetXaxis()->SetTitle("#Delta R");
  hist_jet_truthjet_deltaR->GetYaxis()->SetTitle("#jets");
  c20->SaveAs("./Jets/hist_jet_truthjet_deltaR.pdf");

}
