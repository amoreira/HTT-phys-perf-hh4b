void plot_tracks(){

  gStyle->SetOptStat("neou");

  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.h");
  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.C");
  gROOT->ProcessLine("SetAtlasStyle()");

  // Open file
  TFile *f = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/submitDir/hist-mc15_14TeV.301497.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M1200.recon.AOD.e6011_s3185_s3186_r9871.root");

  // Get histos for tracks
  // pt
  TH1F *hist_track_pt = (TH1F*)f->Get("hist_track_pt");
  // eta
  TH1F *hist_track_eta = (TH1F*)f->Get("hist_track_eta");
  // phi
  TH1F *hist_track_phi = (TH1F*)f->Get("hist_track_phi");
  // d0
  TH1F *hist_track_d0 = (TH1F*)f->Get("hist_track_d0");
  // z0
  TH1F *hist_track_z0 = (TH1F*)f->Get("hist_track_z0");
  // qOverP
  TH1F *hist_track_qOverP = (TH1F*)f->Get("hist_track_qOverP");
  // theta
  TH1F *hist_track_theta = (TH1F*)f->Get("hist_track_theta");
  
  TH2F *hist_track_pt_eta = (TH2F*)f->Get("hist_track_pt_eta");

  // Draw histos
  TCanvas *c1 = new TCanvas();
  hist_track_pt->Draw("hist");
  c1->SetLogy();
  hist_track_pt->GetYaxis()->SetTitle("# tracks");
  hist_track_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  c1->SaveAs("./Tracks/hist_track_pt.pdf");
  /*TPaveStats* stat = (TPaveStats*) hist_track_pt->FindObject("stats");
  stat->SetX1NDC(0.70);
  stat->SetX2NDC(0.89);
  stat->SetY1NDC(0.75);
  stat->SetY2NDC(0.90);
  */
  TCanvas *c2 = new TCanvas();
  hist_track_eta->Draw("hist");
  hist_track_eta->GetYaxis()->SetTitle("# tracks");
  hist_track_eta->GetXaxis()->SetTitle("#eta");
  c2->SaveAs("./Tracks/hist_track_eta.pdf");

  TCanvas *c3 = new TCanvas();
  hist_track_phi->Draw("hist");
  hist_track_phi->GetYaxis()->SetTitle("# tracks");
  hist_track_phi->GetXaxis()->SetTitle("#phi");
  c3->SaveAs("./Tracks/hist_track_phi.pdf");

  TCanvas *c4 = new TCanvas();
  hist_track_d0->Draw("hist");
  //c4->SetLogy();
  hist_track_d0->GetYaxis()->SetTitle("# tracks");
  hist_track_d0->GetXaxis()->SetTitle("d0");
  c4->SaveAs("./Tracks/hist_track_d0.pdf");

  TCanvas *c5 = new TCanvas();
  hist_track_z0->Draw("hist");
  //c5->SetLogy();
  hist_track_z0->GetYaxis()->SetTitle("# tracks");
  hist_track_z0->GetXaxis()->SetTitle("z0");
  c5->SaveAs("./Tracks/hist_track_z0.pdf");

  TCanvas *c6 = new TCanvas();
  hist_track_qOverP->Draw("hist");
  //c6->SetLogy();
  hist_track_qOverP->GetYaxis()->SetTitle("# tracks");
  hist_track_qOverP->GetXaxis()->SetTitle("q/p");
  c6->SaveAs("./Tracks/hist_track_qOverP.pdf");

  TCanvas *c7 = new TCanvas();
  hist_track_theta->Draw("hist");
  //c6->SetLogy();
  hist_track_theta->GetYaxis()->SetTitle("# tracks");
  hist_track_theta->GetXaxis()->SetTitle("#theta");
  c7->SaveAs("./Tracks/hist_track_theta.pdf");

  TCanvas *c8 = new TCanvas();
  hist_track_pt_eta->Draw("colz");
  //c6->SetLogy();
  hist_track_pt_eta->GetYaxis()->SetTitle("#eta");
  hist_track_pt_eta->GetXaxis()->SetTitle("p_{T} [GeV]");
  c8->SaveAs("./Tracks/hist_track_pt_eta.pdf");
  
  // Get histos for tracks
  // pt
  TH1F *hist_track_pt_sel = (TH1F*)f->Get("hist_track_pt_sel");
  // eta
  TH1F *hist_track_eta_sel = (TH1F*)f->Get("hist_track_eta_sel");
  
  // Draw histos
  TCanvas *c9 = new TCanvas();
  hist_track_pt_sel->Draw("hist");
  c9->SetLogy();
  hist_track_pt_sel->GetYaxis()->SetTitle("# tracks");
  hist_track_pt_sel->GetXaxis()->SetTitle("p_{T} [GeV]");
  c9->SaveAs("./Tracks/hist_track_pt_sel.pdf");
 
  TCanvas *c10 = new TCanvas();
  hist_track_eta_sel->Draw("hist");
  hist_track_eta_sel->GetYaxis()->SetTitle("# tracks");
  hist_track_eta_sel->GetXaxis()->SetTitle("#eta");
  c10->SaveAs("./Tracks/hist_track_eta_sel.pdf");

}
