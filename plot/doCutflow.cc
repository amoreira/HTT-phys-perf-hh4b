 // Function that reads cutflow histogram and write bin content to file
#include <iostream>
#include <fstream>

using namespace std;

void doCutflow(TH1F *hist){

  ofstream file;
  file.open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/plot/cutflow.csv", ofstream::out | ofstream::trunc);

  if(!file) cout<<"Error opening file"<<endl;

  double N = hist->GetBinContent(1);

  // Loop over histogram bins
  for(int i = 0; i < hist->GetNbinsX(); i++){
    
    double bin = i+1;
    double bin_content = hist->GetBinContent(bin);

    double eff = (bin_content/N)*100;

    file<<bin<<","<<bin_content<<","<<eff<<endl;

  }

  file.close();

}
