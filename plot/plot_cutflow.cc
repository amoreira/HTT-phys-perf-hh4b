
void plot_cutflow(){

  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.h");
  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.C");
  gROOT->ProcessLine("SetAtlasStyle()");

  TPaveText *t_chain1 = new TPaveText(0.2,0.8,0.4,0.9,"NDC");
  t_chain1->AddText("HLT_b225_L1_j100");
  t_chain1->SetFillStyle(0);
  t_chain1->SetLineWidth(0);
  t_chain1->SetBorderSize(0);

  TPaveText *t_chain2 = new TPaveText(0.2,0.8,0.4,0.9,"NDC");
  t_chain2->AddText("HLT_2b35_2j35_L1_4j15");
  t_chain2->SetFillStyle(0);
  t_chain2->SetLineWidth(0);
  t_chain2->SetBorderSize(0);

  TPaveText *t_chain3 = new TPaveText(0.2,0.8,0.4,0.9,"NDC");
  t_chain3->AddText("HLT_2b55_j100_L1_j75_3j20");
  t_chain3->SetFillStyle(0);
  t_chain3->SetLineWidth(0);
  t_chain3->SetBorderSize(0);

  TPaveText *t_process = new TPaveText(0.7,0.8,0.9,0.9,"NDC");
  t_process->AddText("Signal (hh#rightarrow4b)");
  t_process->SetFillStyle(0);
  t_process->SetLineWidth(0);
  t_process->SetBorderSize(0);

  TFile *f = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/submitDir/hist-mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871.root");
  TFile *f_ftk = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/submitDirFTK/hist-mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871.root");

  TH1F *h_chain1 = (TH1F*)f->Get("hist_trig_HLT_b225_L1_j100");  
  TH1F *h_chain1_ftk = (TH1F*)f_ftk->Get("hist_trig_HLT_b225_L1_j100");

  TH1F *h_chain2 = (TH1F*)f->Get("hist_trig_HLT_2b35_2j35_L1_4j15");  
  TH1F *h_chain2_ftk = (TH1F*)f_ftk->Get("hist_trig_HLT_2b35_2j35_L1_4j15");

  TH1F *h_chain3 = (TH1F*)f->Get("hist_trig_HLT_2b55_j100_L1_j75_3j20");  
  TH1F *h_chain3_ftk = (TH1F*)f_ftk->Get("hist_trig_HLT_2b55_j100_L1_j75_3j20");

  TH1F* h_cutflow = (TH1F*)f->Get("hist_cutflow");
  TH1F* h_cutflow_ftk = (TH1F*)f_ftk->Get("hist_cutflow");

  h_chain1_ftk->SetLineColor(kRed);
  h_chain2_ftk->SetLineColor(kRed);
  h_chain3_ftk->SetLineColor(kRed);
  h_cutflow_ftk->SetLineColor(kRed);

  double factor =1.2;
  // HLT_b225_L1_j100
  TCanvas *c1 = new TCanvas();
  h_chain1->Draw("hist e1");
  h_chain1->SetMaximum(h_chain1->GetMaximum()*1.2);
  h_chain1_ftk->Draw("same hist e1");
  h_chain1->SetMinimum(10);
  
  h_chain1->SetMarkerSize(0);
  h_chain1_ftk->SetMarkerSize(0);

  h_chain1->GetXaxis()->SetBinLabel(1,"Total");
  h_chain1->GetXaxis()->SetBinLabel(2,"#geq j100, |#eta|<2.5");
  h_chain1->GetXaxis()->SetBinLabel(3,"#geq j225");
  h_chain1->GetXaxis()->SetBinLabel(4,"#geq b225");

  TLegend *leg1 = new TLegend(0.2,0.2,0.4,0.4);
  leg1->AddEntry(h_chain1, "Offline b-tagging","l");
  leg1->AddEntry(h_chain1_ftk, "FTK b-tagging","l");
  leg1->SetFillStyle(0);
  leg1->SetLineWidth(0);
  leg1->SetBorderSize(0);
  leg1->Draw();

  t_chain1->Draw();
  t_process->Draw();

  c1->SaveAs("hist_trig_HLT_b225_L1_j100.pdf");

  // HLT_2b35_2j35_L1_4j15
  TCanvas *c2 = new TCanvas();
  h_chain2->Draw("hist e1");
  h_chain2->SetMaximum(h_chain2->GetMaximum()*1.2);
  h_chain2_ftk->Draw("same hist e1");
  h_chain2->SetMinimum(10);
  
  h_chain2->SetMarkerSize(0);
  h_chain2_ftk->SetMarkerSize(0);

  h_chain2->GetXaxis()->SetBinLabel(1,"Total");
  h_chain2->GetXaxis()->SetBinLabel(2,"#geq 4j15, |#eta|<2.5");
  h_chain2->GetXaxis()->SetBinLabel(3,"#geq 4j35");
  h_chain2->GetXaxis()->SetBinLabel(4,"#geq 2b35");

  TLegend *leg2 = new TLegend(0.2,0.2,0.4,0.4);
  leg2->AddEntry(h_chain2, "Offline b-tagging","l");
  leg2->AddEntry(h_chain2_ftk, "FTK b-tagging","l");
  leg2->SetFillStyle(0);
  leg2->SetLineWidth(0);
  leg2->SetBorderSize(0);
  leg2->Draw();

  t_chain2->Draw();
  t_process->Draw();

  c2->SaveAs("hist_trig_HLT_2b35_2j35_L1_4j15.pdf");

  // HLT_2b55_j100_L1_j75_3j20
  TCanvas *c3 = new TCanvas();
  h_chain3->Draw("hist e1");
  h_chain3->SetMaximum(h_chain3->GetMaximum()*1.2);
  h_chain3_ftk->Draw("same hist e1");
  h_chain3->SetMinimum(10);

  h_chain3->SetMarkerSize(0);
  h_chain3_ftk->SetMarkerSize(0);

  h_chain3->GetXaxis()->SetBinLabel(1,"Total");
  h_chain3->GetXaxis()->SetBinLabel(2,"");
  h_chain3->GetXaxis()->SetBinLabel(3,"");
  h_chain3->GetXaxis()->SetBinLabel(4,"");
  
  TLegend *leg3 = new TLegend(0.2,0.2,0.4,0.4);
  leg3->AddEntry(h_chain3, "Offline b-tagging","l");
  leg3->AddEntry(h_chain3_ftk, "FTK b-tagging","l");
  leg3->SetFillStyle(0);
  leg3->SetLineWidth(0);
  leg3->SetBorderSize(0);
  leg3->Draw();

  t_chain3->Draw();
  t_process->Draw();

  c3->SaveAs("hist_trig_HLT_2b55_j100_L1_j75_3j20.pdf");

  // Cutflow all
  TCanvas *c4 = new TCanvas();
  h_cutflow->Draw("hist e1");
  //h_cutflow->SetMaximum(h_cutflow->GetMaximum()*1.2);
  h_cutflow_ftk->Draw("same hist e1");
  //h_cutflow->SetMinimum(10);

  h_cutflow->SetMarkerSize(0);
  h_cutflow_ftk->SetMarkerSize(0);

  h_cutflow->GetXaxis()->SetBinLabel(1,"Total");
  h_cutflow->GetXaxis()->SetBinLabel(2,"Trigger");
  h_cutflow->GetXaxis()->SetBinLabel(3,"#geq 4b40");
  h_cutflow->GetXaxis()->SetBinLabel(4,"1 Higgs candidate");
  h_cutflow->GetXaxis()->SetBinLabel(5,"p_{T}^{lead}>0.5*m_{4j}-103");
  h_cutflow->GetXaxis()->SetBinLabel(6,"p_{T}^{sublead}>0.33*m_{4j}-73");
  h_cutflow->GetXaxis()->SetBinLabel(7,"|#Delta#eta(hh)|<1.5");
  h_cutflow->GetXaxis()->SetBinLabel(8,"X_{hh}<1.6");
  h_cutflow->GetXaxis()->SetBinLabel(9,"X_{Wt}>1.5");  

  TLegend *leg4 = new TLegend(0.2,0.2,0.4,0.4);
  leg4->AddEntry(h_cutflow, "Offline b-tagging","l");
  leg4->AddEntry(h_cutflow_ftk, "FTK b-tagging","l");
  leg4->SetFillStyle(0);
  leg4->SetLineWidth(0);
  leg4->SetBorderSize(0);
  leg4->Draw();

  c4->SetLogy();
  c4->SaveAs("hist_cutflow.pdf");
  
}
