
using namespace std;

void plot_perf_ltagging(){

  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.h");
  gROOT->LoadMacro("/afs/cern.ch/work/a/amoreira/AtlasStyle.C");
  gROOT->ProcessLine("SetAtlasStyle()");

  // Open files
  TFile *f = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/L-TagEfficiency.root");
  TFile *f_ftk = TFile::Open("/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/run/L-TagEfficiency_FTKlike.root");

  TCanvas *c1 = new TCanvas();
  // Get eta plots
  TGraph *gr_eta = (TGraph*)f->Get("TagEfficiency_eta");
  TGraph *gr_ftk_eta = (TGraph*)f_ftk->Get("TagEfficiency_eta");
  // Set colors
  gr_eta->SetLineColor(kRed);
  gr_eta->SetLineWidth(2);
  gr_ftk_eta->SetLineWidth(2);

  // Create multigraph
  TMultiGraph *mgr_eta = new TMultiGraph();
  mgr_eta->Add(gr_eta);
  mgr_eta->Add(gr_ftk_eta);
  mgr_eta->GetXaxis()->SetTitle("|#eta|");
  mgr_eta->GetYaxis()->SetTitle("L mistag probability");  

  mgr_eta->Draw("AL");

  TLegend *leg1 = new TLegend(0.6,0.7,0.9,0.9);
  leg1->AddEntry(gr_eta,"Upgrade offline","l");
  leg1->AddEntry(gr_ftk_eta,"FTK","l");
  leg1->SetFillStyle(0);
  leg1->SetLineWidth(0);
  leg1->SetBorderSize(0);
  leg1->Draw();

  c1->SaveAs("LTagEfficiency_eta.pdf");

  TCanvas *c2 = new TCanvas();
  // Get pt plots
  TGraph *gr_pt = (TGraph*)f->Get("TagEfficiency_pt");
  TGraph *gr_ftk_pt = (TGraph*)f_ftk->Get("TagEfficiency_pt");
  // Set colors
  gr_pt->SetLineColor(kRed);
  gr_pt->SetLineWidth(2);
  gr_ftk_pt->SetLineWidth(2);
  // Create multigraph
  TMultiGraph *mgr_pt = new TMultiGraph();
  mgr_pt->Add(gr_pt);
  mgr_pt->Add(gr_ftk_pt);
  mgr_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
  mgr_pt->GetYaxis()->SetTitle("L mistag probability");  
  
  mgr_pt->Draw("AL");

  TLegend *leg2 = new TLegend(0.6,0.4,0.9,0.6);
  leg2->AddEntry(gr_pt,"Upgrade offline","l");
  leg2->AddEntry(gr_ftk_pt,"FTK","l");
  leg2->SetFillStyle(0);
  leg2->SetLineWidth(0);
  leg2->SetBorderSize(0);
  leg2->Draw();

  c2->SaveAs("LTagEfficiency_pt.pdf");

  // Get 2d plots
  TH2F *map = (TH2F*)f->Get("TagEfficiency2D");
  TH2F *map_ftk = (TH2F*)f_ftk->Get("TagEfficiency2D");
  
  TCanvas *c3 = new TCanvas();
  map->Draw("colz");
  c3->SaveAs("LTagefficiency2d.pdf");
  
  TCanvas *c4 = new TCanvas();
  map_ftk->Draw("colz");
  c4->SaveAs("LTagEfficiency2D_ftk.pdf");
  
  
  return;

}
