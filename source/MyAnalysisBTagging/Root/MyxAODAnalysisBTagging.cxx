#include <AsgTools/MessageCheck.h>
#include <MyAnalysisBTagging/MyxAODAnalysisBTagging.h>
// xAOD core
#include <xAODCore/AuxContainerBase.h>
// xAOD base 
#include <xAODBase/IParticle.h>
#include <xAODBase/IParticleContainer.h>
// EventInfo
#include <xAODEventInfo/EventInfo.h>
// Tracking
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/TrackParticle.h>
#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <xAODTracking/Vertex.h>
// Btagging
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <xAODBTagging/BTagging.h>
#include <xAODBTaggingEfficiency/BTaggingSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"

//#include <xAODBTagging/BTagInfo.h>
// Jets
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
// Jet cleaning tool
#include <JetInterface/IJetSelector.h>
// Jet calibration
#include <JetCalibTools/IJetCalibrationTool.h>
// Truth
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticle.h>
// QuickAna
#include <QuickAna/IQuickAna.h>
// ROOT includes
#include <TH1F.h>
#include <TH2F.h>
#include <TMath.h>
#include <TRandom3.h>

#include <AthLinks/ElementLink.h>

// Upgrade functions
#include "UpgradePerformanceFunctions/UpgradePerformanceFunctions.h"

#include <unistd.h>
#include <stdio.h>
#include <limits.h>

#include <PathResolver/PathResolver.h>

#include <AsgTools/AnaToolHandle.h>

#include "EventLoop/Worker.h"

using namespace std;
using namespace Upgrade;

MyxAODAnalysisBTagging :: MyxAODAnalysisBTagging (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
    m_quickAna ("ana::QuickAna/quickAna",this)

{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  // Declare tools

  //declareProperty("OutputFileName",m_outputStreamName = "ANALYSIS);

}



StatusCode MyxAODAnalysisBTagging :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO("in initialize");
  
  m_upgrade = std::unique_ptr<UpgradePerformanceFunctions>( new UpgradePerformanceFunctions("UpgradePerformanceFunctions") );
  ANA_CHECK( m_upgrade->setProperty("FlavourTaggingCalibrationFile", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/CalibArea-00-01/flavor_tags_v2.0.root") );
  ANA_CHECK( m_upgrade->setProperty("Layout", UpgradePerformanceFunctions::UpgradeLayout::Step1p6) );
  ANA_CHECK( m_upgrade->setProperty("AvgMu", 200) );
  ANA_CHECK( m_upgrade->setProperty("PileupPath", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/") );
  ANA_CHECK( m_upgrade->setProperty("UseHGTD0", false) );
  ANA_CHECK( m_upgrade->initialize() );

  // Get graphs that parameterize FTK b-tag efficiency wrt offline
  std::string filename = "Dataset.csv";
  std::string fileLocation;

  fileLocation = PathResolver::find_file (filename, "DATAPATH", PathResolver::RecursiveSearch);

  const char* format = "%lg %lg";
  gr = new TGraph(fileLocation.c_str(),format,"\t,;");

  std::string filename_forward = "Dataset_forward.csv";
  std::string fileLocation_forward;

  fileLocation_forward = PathResolver::find_file (filename_forward, "DATAPATH", PathResolver::RecursiveSearch);

  gr_forward = new TGraph(fileLocation_forward.c_str(),format,"\t,;");

  // Book histograms
  // Offline
  ANA_CHECK(book(TH1F("hist_cutflow","hist_cutflow",9,-.5,8.5)));
  ANA_CHECK(book(TH1F("hist_jet_pt","hist_jet_pt",100,0,800)));
  ANA_CHECK(book(TH1F("hist_jet_N","hist_jet_N",10,-.5,9.5)));
  ANA_CHECK(book(TH1F("hist_h1_M","hist_h1_M",100,0,250)));
  ANA_CHECK(book(TH1F("hist_h2_M","hist_h2_M",100,0,250)));
  ANA_CHECK(book(TH2F("hist_h1h2_M","hist_h1h2_M",100,0,250,100,0,250)));
  ANA_CHECK(book(TH1F("hist_h1_M_afterXwt","hist_h1_M_afterXwt",100,0,250)));
  ANA_CHECK(book(TH1F("hist_h2_M_afterXwt","hist_h2_M_afterXwt",100,0,250)));
  ANA_CHECK(book(TH2F("hist_h1h2_M_afterXwt","hist_h1h2_M_afterXwt",100,0,250,100,0,250)));
  ANA_CHECK(book(TH1F("hist_HiggsPairs_N","hist_HiggsPairs_N",5,-.5,4.5)));
  ANA_CHECK(book(TH1F("hist_Xhh","hist_Xhh",100,0,10)));
  ANA_CHECK(book(TH1F("hist_jets_no_btag_N","hist_jets_no_btag_N",5,-.5,4.5)));
  ANA_CHECK(book(TH1F("hist_Xwt","hist_Xwt",100,0,200)));
  ANA_CHECK(book(TH1F("hist_trigger_1bj225_pt","hist_trigger_1bj225_pt",100,0,500)));
  ANA_CHECK(book(TH1F("hist_trigger_2bj35_55_pt","hist_trigger_2bj35_55_pt",100,0,500)));
  ANA_CHECK(book(TH2F("hist_dRjjlead_m4j","hist_dRjjlead_m4j",100,0,2000,100,0,5)));
  ANA_CHECK(book(TH1F("hist_Dhh","hist_Dhh",100,0,10)));
  // FTK
  ANA_CHECK(book(TH1F("hist_cutflow_FTK","hist_cutflow_FTK",9,-.5,8.5)));
  ANA_CHECK(book(TH1F("hist_jet_pt_FTK","hist_jet_pt_FTK",100,0,800)));
  ANA_CHECK(book(TH1F("hist_jet_N_FTK","hist_jet_N_FTK",10,-.5,9.5)));
  ANA_CHECK(book(TH1F("hist_h1_M_FTK","hist_h1_M_FTK",100,0,250)));
  ANA_CHECK(book(TH1F("hist_h2_M_FTK","hist_h2_M_FTK",100,0,250)));
  ANA_CHECK(book(TH2F("hist_h1h2_M_FTK","hist_h1h2_M_FTK",100,0,250,100,0,250)));
  ANA_CHECK(book(TH1F("hist_h1_M_afterXwt_FTK","hist_h1_M_afterXwt_FTK",100,0,250)));
  ANA_CHECK(book(TH1F("hist_h2_M_afterXwt_FTK","hist_h2_M_afterXwt_FTK",100,0,250)));
  ANA_CHECK(book(TH2F("hist_h1h2_M_afterXwt_FTK","hist_h1h2_M_afterXwt_FTK",100,0,250,100,0,250)));
  ANA_CHECK(book(TH1F("hist_HiggsPairs_N_FTK","hist_HiggsPairs_N_FTK",5,-.5,4.5)));
  ANA_CHECK(book(TH1F("hist_Xhh_FTK","hist_Xhh_FTK",100,0,10)));
  ANA_CHECK(book(TH1F("hist_jets_no_btag_N_FTK","hist_jets_no_btag_N_FTK",5,-.5,4.5)));
  ANA_CHECK(book(TH1F("hist_Xwt_FTK","hist_Xwt_FTK",100,0,200)));
  ANA_CHECK(book(TH1F("hist_trigger_1bj225_pt_FTK","hist_trigger_1bj225_pt_FTK",100,0,500)));
  ANA_CHECK(book(TH1F("hist_trigger_2bj35_55_pt_FTK","hist_trigger_2bj35_55_pt_FTK",100,0,500)));
  ANA_CHECK(book(TH2F("hist_dRjjlead_m4j_FTK","hist_dRjjlead_m4j_FTK",100,0,2000,100,0,5)));
  ANA_CHECK(book(TH1F("hist_Dhh_FTK","hist_Dhh_FTK",100,0,10)));

  ANA_CHECK(book(TH1F("hist_trig","hist_trig",4,-.5,3.5)));

  // Output tree
  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);
  
  m_jetPt = new vector<float>();
  mytree->Branch("JetPt", &m_jetPt);

  m_jetEta = new vector<float>();
  mytree->Branch("JetEta", &m_jetEta);

  m_jetPhi = new vector<float>();
  mytree->Branch("JetPhi", &m_jetPhi);

  mytree->Branch("jetN",&m_jetN);

  mytree->Branch("h1Pt", &m_h1Pt);
  mytree->Branch("h2Pt", &m_h2Pt);

  mytree->Branch("h1Eta", &m_h1Eta);
  mytree->Branch("h2Eta", &m_h2Eta);

  mytree->Branch("h1Phi", &m_h1Phi);
  mytree->Branch("h2Phi", &m_h2Phi);

  mytree->Branch("h1M", &m_h1M);
  mytree->Branch("h2M", &m_h2M);

  // For FTK
  m_jetPt_FTK = new vector<float>();
  mytree->Branch("JetPtFTK", &m_jetPt_FTK);

  m_jetEta_FTK = new vector<float>();
  mytree->Branch("JetEtaFTK", &m_jetEta_FTK);

  m_jetPhi_FTK = new vector<float>();
  mytree->Branch("JetPhiFTK", &m_jetPhi_FTK);

  mytree->Branch("jetNFTK",&m_jetN_FTK);

  mytree->Branch("h1PtFTK", &m_h1Pt_FTK);
  mytree->Branch("h2PtFTK", &m_h2Pt_FTK);

  mytree->Branch("h1EtaFTK", &m_h1Eta_FTK);
  mytree->Branch("h2EtaFTK", &m_h2Eta_FTK);

  mytree->Branch("h1PhiFTK", &m_h1Phi_FTK);
  mytree->Branch("h2PhiFTK", &m_h2Phi_FTK);

  mytree->Branch("h1MFTK", &m_h1M_FTK);
  mytree->Branch("h2MFTK", &m_h2M_FTK);

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysisBTagging :: execute(){
 
  ANA_MSG_INFO("in execute");

  // Retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo,"EventInfo"));

  // Print out run and event number from retrieved object
  ANA_MSG_INFO("in execute, runNumber="<<eventInfo->runNumber()<<",eventNumber="<<eventInfo->eventNumber());

  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
 
  // Tell QuickAna to process event
  ANA_CHECK (m_quickAna->process());
  hist("hist_cutflow")->Fill(0);
  hist("hist_cutflow_FTK")->Fill(0);

  // Set value of variables
  m_jetN = -999.;
  m_h1Pt = -999.;
  m_h2Pt = -999.;
  m_h1M = -999.;
  m_h2M = -999.;
  m_h1Eta = -999.;
  m_h2Eta = -999.;
  m_h1Phi = -999.;
  m_h2Phi = -999.;

  m_jetN_FTK = -999.;
  m_h1Pt_FTK = -999.;
  m_h2Pt_FTK = -999.;
  m_h1M_FTK = -999.;
  m_h2M_FTK = -999.;
  m_h1Eta_FTK = -999.;
  m_h2Eta_FTK = -999.;
  m_h1Phi_FTK = -999.;
  m_h2Phi_FTK = -999.;

  // Return info about which trigger chains passed pre selection
  int passed_HLT_2b35_2j35_L1_4j15_presel = eventInfo->auxdata<int>("pass_HLT_2b35_2j35_L1_4j15");
  int passed_HLT_b225_L1_j100_presel = eventInfo->auxdata<int>("pass_HLT_2b35_2j35_L1_4j15");
  int passed_HLT_2b55_j100_L1_j75_3j20_presel = eventInfo->auxdata<int>("pass_HLT_2b35_2j35_L1_4j15");

  //------------------------------------------
  // Retrieve collections
  //------------------------------------------

  // Reco anti-kt R=0.4 jets
  const xAOD::JetContainer *antikt4EMPFjets =  nullptr;
  ANA_CHECK(evtStore()->retrieve(antikt4EMPFjets,"AntiKt4EMPFlowJets"));

  auto selJets = std::make_unique<xAOD::JetContainer>();
  auto selJetsAux = std::make_unique<xAOD::AuxContainerBase>();
  selJets->setStore (selJetsAux.get()); //< Connect the two

  auto selJets_no_btag = std::make_unique<xAOD::JetContainer>();
  auto selJetsAux_no_btag = std::make_unique<xAOD::AuxContainerBase>();
  selJets_no_btag->setStore (selJetsAux_no_btag.get()); //< Connect the two

  int evNo = eventInfo->eventNumber();
  TRandom3 *rand = new TRandom3(evNo);
  
  char jetlabel = 'L';
  // Do offline b tagging on jet collection
  for(const xAOD::Jet *jet : *antikt4EMPFjets){
  
    int flavor = jet->getAttribute<int>("HadronConeExclTruthLabelID");
    
    if(flavor == 0){
      jetlabel = 'L';
    }else if(flavor == 5){
      jetlabel = 'B';
    }else if(flavor == 4){
      jetlabel = 'C';
    }
    double tagEff = m_upgrade->getFlavourTagEfficiency(jet->pt(), jet->eta(), jetlabel, "mv2c10", 70, false);
    
    double prob = rand->Rndm();
    if(prob<tagEff){
      
      xAOD::Jet* selJet = new xAOD::Jet();
      selJets->push_back (selJet); // jet acquires the goodJets auxstore
      *selJet = *jet; // copies auxdata from one auxstore to the other
      
    }else{
      
      xAOD::Jet* selJet_no_btag = new xAOD::Jet();
      selJets_no_btag->push_back (selJet_no_btag); // jet acquires the goodJets auxstore
      *selJet_no_btag = *jet; // copies auxdata from one auxstore to the other
      
    }
    
  }

  ANA_CHECK (evtStore()->record (selJets.release(), "SelJets"));
  ANA_CHECK (evtStore()->record (selJetsAux.release(), "SelJetsAux."));

  ANA_CHECK (evtStore()->record (selJets_no_btag.release(), "SelJetsNoBTag"));
  ANA_CHECK (evtStore()->record (selJetsAux_no_btag.release(), "SelJetsNoBTagAux."));

  // Selected jets (jet cleaning, pt>20, eta<4.5 and truth level b tag using upgrade functions)
  const xAOD::JetContainer *sel_jet = nullptr;
  ANA_CHECK(evtStore()->retrieve(sel_jet,"SelJets"));
  // Selected jets (jet cleaning, pt>20, eta<4.5 and no-btag)
  const xAOD::JetContainer *sel_jet_no_btag = nullptr;
  ANA_CHECK(evtStore()->retrieve(sel_jet_no_btag,"SelJetsNoBTag"));
  
  bool pass_chain1 = false;
  bool pass_chain2 = false;
  bool pass_chain3 = false;

  bool pass_chain1_ftk = false;
  bool pass_chain2_ftk = false;
  bool pass_chain3_ftk = false;

  if(passed_HLT_b225_L1_j100_presel){

    pass_chain1 = pass_HLT_b225_L1_j100(0,antikt4EMPFjets,eventInfo);
    pass_chain1_ftk = pass_HLT_b225_L1_j100(1,antikt4EMPFjets,eventInfo);

  }

  if(passed_HLT_2b35_2j35_L1_4j15_presel){

    pass_chain2 = pass_HLT_2b35_2j35_L1_4j15(0,antikt4EMPFjets,eventInfo);
    pass_chain2_ftk = pass_HLT_2b35_2j35_L1_4j15(1,antikt4EMPFjets,eventInfo);

  }
  if(passed_HLT_2b55_j100_L1_j75_3j20_presel){

    pass_chain3 = pass_HLT_2b55_j100_L1_j75_3j20(0,antikt4EMPFjets,eventInfo);
    pass_chain3_ftk = pass_HLT_2b55_j100_L1_j75_3j20(1,antikt4EMPFjets,eventInfo);

  }

  bool pass_off = false;
  bool pass_ftk = false;

  if(pass_chain1 || pass_chain2 || pass_chain3){

    ANA_MSG_INFO("Passed offline trigger");
    pass_off = true;

    hist("hist_cutflow")->Fill(1);
    StatusCode offline = offline_analysis(0,sel_jet,sel_jet_no_btag);

  }

  if(pass_chain1_ftk || pass_chain2_ftk || pass_chain3_ftk){

    ANA_MSG_INFO("Passed FTK trigger");
    pass_ftk = true;

    hist("hist_cutflow_FTK")->Fill(1);    
    StatusCode offline_ftk = offline_analysis(1,sel_jet,sel_jet_no_btag);

  }

  if(pass_off && !pass_ftk){

    hist("hist_trig")->Fill(0);
    
  }else if(!pass_off && pass_ftk){

    hist("hist_trig")->Fill(1);

  }else if(pass_off && pass_ftk){

    hist("hist_trig")->Fill(2);

  }else if(!pass_off && !pass_ftk){

    hist("hist_trig")->Fill(3);

  }

  ANA_MSG_INFO("----------------");

  tree("analysis")->Fill();

  return StatusCode::SUCCESS;

}

bool MyxAODAnalysisBTagging::pass_HLT_b225_L1_j100(int tagging, const xAOD::JetContainer *jet_col, const xAOD::EventInfo *EventInfo){

  int evNo = EventInfo->eventNumber();
  TRandom3 *rand = new TRandom3(evNo);
  
  bool pass = false;

  int countHLT_b225 = 0;

  char jetlabel = 'L';
  for(const xAOD::Jet *jet : *jet_col){

    if(jet->pt()*0.001<225.) continue;
    countHLT_b225 += 1;

    int flavor = jet->getAttribute<int>("HadronConeExclTruthLabelID");
      
    if(flavor == 0){
      jetlabel = 'L';
    }else if(flavor == 5){
      jetlabel = 'B';
    }else if(flavor == 4){
      jetlabel = 'C';
    }
    double tagEff = m_upgrade->getFlavourTagEfficiency(jet->pt(), jet->eta(), jetlabel, "mv2c10", 70, false);
    if(tagging == 1) tagEff *= getFTK(tagEff, jet->eta(), jetlabel);
    
    double prob = rand->Rndm();
    if(prob<tagEff){

      countHLT_b225 += 1; 
    }
  } // End of loop over jets

  if(countHLT_b225 >= 1) pass = true;

  return pass;
}

bool MyxAODAnalysisBTagging::pass_HLT_2b55_j100_L1_j75_3j20(int tagging, const xAOD::JetContainer *jet_col, const xAOD::EventInfo *EventInfo){

  int evNo = EventInfo->eventNumber();
  TRandom3 *rand = new TRandom3(evNo);

  bool pass = false;

  int countHLT_2b55 = 0;

  char jetlabel = 'L';
  for(const xAOD::Jet *jet : *jet_col){

    // We want at least 2 b-tagged jets with pT>55.
    if(jet->pt()*0.001<55.) continue;
    
    int flavor = jet->getAttribute<int>("HadronConeExclTruthLabelID");
    
    if(flavor == 0){
      jetlabel = 'L';
    }else if(flavor == 5){
      jetlabel = 'B';
    }else if(flavor == 4){
      jetlabel = 'C';
    }
    double tagEff = m_upgrade->getFlavourTagEfficiency(jet->pt(), jet->eta(), jetlabel, "mv2c10", 70, false);
    if(tagging == 1) tagEff *= getFTK(tagEff, jet->eta(), jetlabel);
    
    double prob = rand->Rndm();
    if(prob<tagEff) countHLT_2b55 += 1;
    
  }

  if(countHLT_2b55 >= 2) pass = true;
  
  return pass;

}

bool MyxAODAnalysisBTagging::pass_HLT_2b35_2j35_L1_4j15(int tagging, const xAOD::JetContainer *jet_col, const xAOD::EventInfo *EventInfo){
  
  int evNo = EventInfo->eventNumber();
  TRandom3 *rand = new TRandom3(evNo);
  
  bool pass = false;

  int countHLT_2b35 = 0;

  char jetlabel = 'L';

  for(const xAOD::Jet *jet : *jet_col){

    if(jet->pt()*0.001<35.) continue;

    int flavor = jet->getAttribute<int>("HadronConeExclTruthLabelID");
    
    if(flavor == 0){ 

      jetlabel = 'L';

    }else if(flavor == 5){

      jetlabel = 'B';

    }else if(flavor == 4){

      jetlabel = 'C';

    }

    double tagEff = m_upgrade->getFlavourTagEfficiency(jet->pt(), jet->eta(), jetlabel, "mv2c10", 70, false);

    if(tagging == 1){

      tagEff *= getFTK(tagEff, jet->eta(), jetlabel);

    }
    
    double prob = rand->Rndm();

    if(prob<tagEff){

      countHLT_2b35 += 1;

    }
    
  } // End of loop over jets

  if(countHLT_2b35 >= 2){

    pass = true;

  }
    
  return pass;
}

double MyxAODAnalysisBTagging::getFTK(double offline_eff, double eta, char jetlabel){

  double FTK_eff_wrt_off = 1.;

  if(jetlabel == 'B'){
    if(TMath::Abs(eta)<1.1){
      FTK_eff_wrt_off = gr->Eval(offline_eff);
      //FTK_eff_wrt_off = 1.;
    }else{
      FTK_eff_wrt_off = gr_forward->Eval(offline_eff);
      //FTK_eff_wrt_off = 1.;
    }
  }else if(jetlabel == 'C'){
    FTK_eff_wrt_off = 2.;
  }else if(jetlabel == 'L'){
    FTK_eff_wrt_off = 2.;
  }
  
  return FTK_eff_wrt_off;

}

StatusCode MyxAODAnalysisBTagging::offline_analysis(int UseFTKjets, const xAOD::JetContainer *sel_jet, const xAOD::JetContainer *sel_jet_no_btag){

  string suf = "";
  if(UseFTKjets==1){
    suf = "_FTK";  
  }else if(UseFTKjets==0){
    suf = "";
  }

  string hist_name = "hist_cutflow"+suf;  
  string hist_name_jet_pt = "hist_jet_pt"+suf;
  string hist_name_jet_N = "hist_jet_N"+suf;
  string hist_name_h1_M = "hist_h1_M"+suf;
  string hist_name_h2_M = "hist_h2_M"+suf;
  string hist_name_h1h2_M = "hist_h1h2_M"+suf;
  string hist_name_h1_M_afterXwt = "hist_h1_M_afterXwt"+suf;
  string hist_name_h2_M_afterXwt = "hist_h2_M_afterXwt"+suf;
  string hist_name_h1h2_M_afterXwt = "hist_h1h2_M_afterXwt"+suf;
  string hist_name_HiggsPairs_N = "hist_HiggsPairs_N"+suf;
  string hist_name_Xhh = "hist_Xhh"+suf;
  string hist_name_jets_no_btag_N = "hist_jets_no_btag_N"+suf; 
  string hist_name_Xwt = "hist_Xwt"+suf;
  string hist_name_trigger_1bj225_pt = "hist_trigger_1bj225_pt"+suf;
  string hist_name_trigger_2bj35_55_pt = "hist_trigger_2bj35_55_pt"+suf;

  string hist_name_Dhh = "hist_Dhh"+suf;

  int passAna_j40=0;

  // Analysis cuts
  // Loop over seleted jets

  if(UseFTKjets==0){

    m_jetPt->clear();
    m_jetEta->clear();
    m_jetEta->clear();

  }else{

    m_jetPt_FTK->clear();
    m_jetEta_FTK->clear();
    m_jetEta_FTK->clear();

  }

  hist(hist_name_jet_N)->Fill(sel_jet->size());

  if(UseFTKjets==0){
    m_jetN = sel_jet->size();
  }else{
    m_jetN_FTK = sel_jet->size();
  }

  for(const xAOD::Jet *jet : *sel_jet){

    if(jet->pt()*0.001>40.) passAna_j40 +=1;

    hist(hist_name_jet_pt)->Fill(jet->pt()*0.001);
    
    if(UseFTKjets==0){
      
      m_jetPt->push_back(jet->pt()*0.001);
      m_jetEta->push_back(jet->eta());
      m_jetPhi->push_back(jet->phi());

    }else{
      
      m_jetPt_FTK->push_back(jet->pt()*0.001);
      m_jetEta_FTK->push_back(jet->eta());
      m_jetPhi_FTK->push_back(jet->phi());

    }
  }

  if(passAna_j40<4) return StatusCode::SUCCESS;
  hist(hist_name)->Fill(2);

  //--------------------------------------
  // Construct Higgs candidates
  //--------------------------------------
  std::vector<std::pair<TLorentzVector,TLorentzVector>> HiggsPairs;
  // Pair 1
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair1_h1_tmp = std::make_pair(sel_jet->at(0),sel_jet->at(1));
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair1_h2_tmp = std::make_pair(sel_jet->at(2),sel_jet->at(3));

  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair1_h1;
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair1_h2;

  double ptSumPair1_h1 = pair1_h1_tmp.first->pt()+pair1_h1_tmp.second->pt();
  double ptSumPair1_h2 = pair1_h2_tmp.first->pt()+pair1_h2_tmp.second->pt();

  if(ptSumPair1_h1>ptSumPair1_h2){

    pair1_h1 = pair1_h1_tmp;
    pair1_h2 = pair1_h2_tmp;
    
  }else{

    pair1_h1 = pair1_h2_tmp;
    pair1_h2 = pair1_h1_tmp;

  } 

  // Pair 2
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair2_h1_tmp = std::make_pair(sel_jet->at(0),sel_jet->at(2));
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair2_h2_tmp = std::make_pair(sel_jet->at(1),sel_jet->at(3));
  
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair2_h1;
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair2_h2;

  double ptSumPair2_h1 = pair2_h1_tmp.first->pt()+pair2_h1_tmp.second->pt();
  double ptSumPair2_h2 = pair2_h2_tmp.first->pt()+pair2_h2_tmp.second->pt();

  if(ptSumPair2_h1>ptSumPair2_h2){

    pair2_h1 = pair2_h1_tmp;
    pair2_h2 = pair2_h2_tmp;
    
  }else{

    pair2_h1 = pair2_h2_tmp;
    pair2_h2 = pair2_h1_tmp;

  } 

  // Pair 3
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair3_h1_tmp = std::make_pair(sel_jet->at(0),sel_jet->at(3));
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair3_h2_tmp = std::make_pair(sel_jet->at(1),sel_jet->at(2));
    
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair3_h1;
  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair3_h2;

  double ptSumPair3_h1 = pair3_h1_tmp.first->pt()+pair3_h1_tmp.second->pt();
  double ptSumPair3_h2 = pair3_h2_tmp.first->pt()+pair3_h2_tmp.second->pt();

  if(ptSumPair3_h1>ptSumPair3_h2){

    pair3_h1 = pair3_h1_tmp;
    pair3_h2 = pair3_h2_tmp;
    
  }else{

    pair3_h1 = pair3_h2_tmp;
    pair3_h2 = pair3_h1_tmp;

  } 

  if(dRpass(pair1_h1,pair1_h2)){

    TLorentzVector *pair1_h1_j1 = new TLorentzVector();
    pair1_h1_j1->SetPtEtaPhiM(pair1_h1.first->pt(),pair1_h1.first->eta(),pair1_h1.first->phi(),pair1_h1.first->m());
    TLorentzVector *pair1_h1_j2 = new TLorentzVector();
    pair1_h1_j2->SetPtEtaPhiM(pair1_h1.second->pt(),pair1_h1.second->eta(),pair1_h1.second->phi(),pair1_h1.second->m());
    TLorentzVector *pair1_h2_j1 = new TLorentzVector();
    pair1_h2_j1->SetPtEtaPhiM(pair1_h2.first->pt(),pair1_h2.first->eta(),pair1_h2.first->phi(),pair1_h2.first->m());
    TLorentzVector *pair1_h2_j2 = new TLorentzVector();
    pair1_h2_j2->SetPtEtaPhiM(pair1_h2.second->pt(),pair1_h2.second->eta(),pair1_h2.second->phi(),pair1_h2.second->m());
    
    TLorentzVector pair1_h1_vec = (*pair1_h1_j1)+(*pair1_h1_j2);
    TLorentzVector pair1_h2_vec = (*pair1_h2_j1)+(*pair1_h2_j2);
    
    HiggsPairs.push_back(std::make_pair(pair1_h1_vec,pair1_h2_vec));
  
  }

  if(dRpass(pair2_h1,pair2_h2)){

    TLorentzVector *pair2_h1_j1 = new TLorentzVector();
    pair2_h1_j1->SetPtEtaPhiM(pair2_h1.first->pt(),pair2_h1.first->eta(),pair2_h1.first->phi(),pair2_h1.first->m());
    TLorentzVector *pair2_h1_j2 = new TLorentzVector();
    pair2_h1_j2->SetPtEtaPhiM(pair2_h1.second->pt(),pair2_h1.second->eta(),pair2_h1.second->phi(),pair2_h1.second->m());
    TLorentzVector *pair2_h2_j1 = new TLorentzVector();
    pair2_h2_j1->SetPtEtaPhiM(pair2_h2.first->pt(),pair2_h2.first->eta(),pair2_h2.first->phi(),pair2_h2.first->m());
    TLorentzVector *pair2_h2_j2 = new TLorentzVector();
    pair2_h2_j2->SetPtEtaPhiM(pair2_h2.second->pt(),pair2_h2.second->eta(),pair2_h2.second->phi(),pair2_h2.second->m());
    
    TLorentzVector pair2_h1_vec = (*pair2_h1_j1)+(*pair2_h1_j2);
    TLorentzVector pair2_h2_vec = (*pair2_h2_j1)+(*pair2_h2_j2);
    
    HiggsPairs.push_back(std::make_pair(pair2_h1_vec,pair2_h2_vec));
  
  }

  if(dRpass(pair3_h1,pair3_h2)){

    TLorentzVector *pair3_h1_j1 = new TLorentzVector();
    pair3_h1_j1->SetPtEtaPhiM(pair3_h1.first->pt(),pair3_h1.first->eta(),pair3_h1.first->phi(),pair3_h1.first->m());
    TLorentzVector *pair3_h1_j2 = new TLorentzVector();
    pair3_h1_j2->SetPtEtaPhiM(pair3_h1.second->pt(),pair3_h1.second->eta(),pair3_h1.second->phi(),pair3_h1.second->m());
    TLorentzVector *pair3_h2_j1 = new TLorentzVector();
    pair3_h2_j1->SetPtEtaPhiM(pair3_h2.first->pt(),pair3_h2.first->eta(),pair3_h2.first->phi(),pair3_h2.first->m());
    TLorentzVector *pair3_h2_j2 = new TLorentzVector();
    pair3_h2_j2->SetPtEtaPhiM(pair3_h2.second->pt(),pair3_h2.second->eta(),pair3_h2.second->phi(),pair3_h2.second->m());
    
    TLorentzVector pair3_h1_vec = (*pair3_h1_j1)+(*pair3_h1_j2);
    TLorentzVector pair3_h2_vec = (*pair3_h2_j1)+(*pair3_h2_j2);
    
    HiggsPairs.push_back(std::make_pair(pair3_h1_vec,pair3_h2_vec));
  
  }

  hist(hist_name_HiggsPairs_N)->Fill(HiggsPairs.size());

  if(HiggsPairs.size()==0) return StatusCode::SUCCESS;

  ANA_MSG_INFO("There is at least one Higgs pair");

  int index = 0;
  if(HiggsPairs.size()>1){

    double Dhh = computeDhh(HiggsPairs[0]);
    double Dhh_tmp;
    // Loop over possible pairs
    for(int i=1; i<HiggsPairs.size(); i++){

      Dhh_tmp = computeDhh(HiggsPairs[i]);
      if(Dhh_tmp<Dhh){
	
	Dhh = Dhh_tmp;
	index = i;

      }

    }
    
    hist(hist_name_Dhh)->Fill(Dhh);

  }

  hist(hist_name)->Fill(3);

  std::pair<TLorentzVector,TLorentzVector> HiggsPair = HiggsPairs[index];
  TLorentzVector HiggsPair_vec = HiggsPair.first+HiggsPair.second;

  if(UseFTKjets==0){
  
    m_h1M = HiggsPair.first.M()*0.001;
    m_h2M = HiggsPair.second.M()*0.001;
    
    m_h1Pt = HiggsPair.first.Pt()*0.001;
    m_h2Pt = HiggsPair.second.Pt()*0.001;
    
    m_h1Eta = HiggsPair.first.Eta();
    m_h2Eta = HiggsPair.second.Eta();
    
    m_h1Phi = HiggsPair.first.Phi();
    m_h2Phi = HiggsPair.second.Phi();
  
  }else{

    m_h1M_FTK = HiggsPair.first.M()*0.001;
    m_h2M_FTK = HiggsPair.second.M()*0.001;
    
    m_h1Pt_FTK = HiggsPair.first.Pt()*0.001;
    m_h2Pt_FTK = HiggsPair.second.Pt()*0.001;
    
    m_h1Eta_FTK = HiggsPair.first.Eta();
    m_h2Eta_FTK = HiggsPair.second.Eta();
    
    m_h1Phi_FTK = HiggsPair.first.Phi();
    m_h2Phi_FTK = HiggsPair.second.Phi();

  }

  hist(hist_name_h1_M)->Fill(HiggsPair.first.M()*0.001);
  hist(hist_name_h1_M)->Fill(HiggsPair.second.M()*0.001);
  hist(hist_name_h1h2_M)->Fill(HiggsPair.first.M()*0.001,HiggsPair.second.M()*0.001);

  if(HiggsPair.first.Pt()*0.001<(0.5*HiggsPair_vec.M()*0.001-103)) return StatusCode::SUCCESS;
  hist(hist_name)->Fill(4);

  if(HiggsPair.second.Pt()*0.001<(0.33*HiggsPair_vec.M()*0.001-73)) return StatusCode::SUCCESS;
  hist(hist_name)->Fill(5);;

  if(TMath::Abs(HiggsPair.first.Eta()-HiggsPair.second.Eta())>1.5) return StatusCode::SUCCESS;
  hist(hist_name)->Fill(6);;

  double Xhh = TMath::Sqrt(TMath::Power((HiggsPair.first.M()*0.001-120)/(0.1*HiggsPair.first.M()*0.001),2)+TMath::Power((HiggsPair.second.M()*0.001-110)/(0.1*HiggsPair.second.M()*0.001),2));
  hist(hist_name_Xhh)->Fill(Xhh);
  
  if(Xhh>1.6) return StatusCode::SUCCESS;
  hist(hist_name)->Fill(7);

  ANA_MSG_INFO("Made it through all analysis cuts except Xwt");

  hist(hist_name_h1_M_afterXwt)->Fill(HiggsPair.first.M()*0.001);
  hist(hist_name_h1_M_afterXwt)->Fill(HiggsPair.second.M()*0.001);
  hist(hist_name_h1h2_M_afterXwt)->Fill(HiggsPair.first.M()*0.001,HiggsPair.second.M()*0.001);
  
  // Construct top candidates from b and light jets
  hist(hist_name_jets_no_btag_N)->Fill(sel_jet_no_btag->size());
  // Get b-jet with highest pT
  const xAOD::Jet *top_b = sel_jet->at(0);
  
  // Xwt variable: helps supress ttbar
  double Xwt = computeXwt(top_b,sel_jet_no_btag);
  hist(hist_name_Xwt)->Fill(Xwt);

  if(Xwt<1.5) return StatusCode::SUCCESS;
  hist(hist_name)->Fill(8);

  // Trigger plots
  hist(hist_name_trigger_1bj225_pt)->Fill(sel_jet->at(0)->pt()*0.001);
  hist(hist_name_trigger_2bj35_55_pt)->Fill(sel_jet->at(0)->pt()*0.001,sel_jet->at(1)->pt()*0.001);

  return StatusCode::SUCCESS;
} // End execute

bool MyxAODAnalysisBTagging::dRpass(std::pair<const xAOD::Jet*,const xAOD::Jet*> pair_h1,  std::pair<const xAOD::Jet*,const xAOD::Jet*> pair_h2){

  bool pass_dRjj_lead_subl = false;

  TLorentzVector *pair_h1_j1 = new TLorentzVector();
  pair_h1_j1->SetPtEtaPhiM(pair_h1.first->pt(),pair_h1.first->eta(),pair_h1.first->phi(),pair_h1.first->m());
  TLorentzVector *pair_h1_j2 = new TLorentzVector();
  pair_h1_j2->SetPtEtaPhiM(pair_h1.second->pt(),pair_h1.second->eta(),pair_h1.second->phi(),pair_h1.second->m());
  TLorentzVector *pair_h2_j1 = new TLorentzVector();
  pair_h2_j1->SetPtEtaPhiM(pair_h2.first->pt(),pair_h2.first->eta(),pair_h2.first->phi(),pair_h2.first->m());
  TLorentzVector *pair_h2_j2 = new TLorentzVector();
  pair_h2_j2->SetPtEtaPhiM(pair_h2.second->pt(),pair_h2.second->eta(),pair_h2.second->phi(),pair_h2.second->m());

  TLorentzVector pair_h1_vec = (*pair_h1_j1)+(*pair_h1_j2);
  TLorentzVector pair_h2_vec = (*pair_h2_j1)+(*pair_h2_j2);
  TLorentzVector pair_hh = pair_h1_vec+pair_h2_vec; 

  double pair_m4j = pair_hh.M()*0.001;
  double pair_dRjj_lead = TMath::Sqrt(TMath::Power(pair_h1_j1->Eta()-pair_h1_j2->Eta(),2)+TMath::Power(pair_h1_j1->Phi()-pair_h1_j2->Phi(),2));
  double pair_dRjj_subl = TMath::Sqrt(TMath::Power(pair_h2_j1->Eta()-pair_h2_j2->Eta(),2)+TMath::Power(pair_h2_j1->Phi()-pair_h2_j2->Phi(),2));

  double dRjj_lead_low = (360./pair_m4j)-0.5;
  double dRjj_lead_up = (653./pair_m4j)+0.475;

  double dRjj_subl_low = (235./pair_m4j);
  double dRjj_subl_up = (875./pair_m4j)+0.35;

  bool pass_dRjj_lead = false;
  bool pass_dRjj_subl = false;

  //hist("hist_dRjjlead_m4j")->Fill(pair_m4j,pair_dRjj_lead);

  if(pair_m4j<1250){

    if(pair_dRjj_lead>dRjj_lead_low && pair_dRjj_lead<dRjj_lead_up) pass_dRjj_lead = true;
    if(pair_dRjj_subl>dRjj_subl_low && pair_dRjj_subl<dRjj_subl_up) pass_dRjj_subl = true;
 

  }else{

    if(pair_dRjj_lead>0 && pair_dRjj_lead<1) pass_dRjj_lead = true;
    if(pair_dRjj_subl>0 && pair_dRjj_subl<1) pass_dRjj_subl = true;
    
  }

  if(pass_dRjj_lead && pass_dRjj_subl){

    pass_dRjj_lead_subl = true;
   
  }else{

    pass_dRjj_lead_subl = false;

  }

  return pass_dRjj_lead_subl;

}

double MyxAODAnalysisBTagging::computeDhh(std::pair<TLorentzVector,TLorentzVector> pairhh){

  double Dhh = TMath::Abs(pairhh.first.M()*0.001-(120./110.)*pairhh.second.M()*0.001)/TMath::Sqrt(1+TMath::Power(120./110.,2));
  //hist("hist_Dhh")->Fill(Dhh);
  return Dhh;

}

double MyxAODAnalysisBTagging::computeXwt(const xAOD::Jet *bjet, const xAOD::JetContainer *light_jets){

  double Xwt = 9e9;
  
  double mt = bjet->m()*0.001;
  double mw;

  if(light_jets->size()==0 || light_jets->size()==1){

    ANA_MSG_INFO("No hadronically decaying top quark candidate in event");

  }else if(light_jets->size()==2){
  
    // Construct W candidate from 2 light jets
    TLorentzVector *w_j1 = new TLorentzVector();
    w_j1->SetPtEtaPhiM(light_jets->at(0)->pt()*0.001,light_jets->at(0)->eta(),light_jets->at(0)->phi(),light_jets->at(0)->m()*0.001);
    TLorentzVector *w_j2 = new TLorentzVector();
    w_j2->SetPtEtaPhiM(light_jets->at(1)->pt()*0.001,light_jets->at(1)->eta(),light_jets->at(1)->phi(),light_jets->at(1)->m()*0.001);
    
    TLorentzVector w = (*w_j1)+(*w_j2);
    mw = w.M();
    
    Xwt = TMath::Sqrt(TMath::Power((mw-80)/(0.1*mw),2)+TMath::Power((mt-173)/(0.1*mt),2));

  }else{ // There are more than 2 light jets

    double Xwt_min = 9e9;
    // Loop over light jets and pair them
    for(int i=0; i<light_jets->size(); i++){

      TLorentzVector *w_j1 = new TLorentzVector();
      w_j1->SetPtEtaPhiM(light_jets->at(i)->pt()*0.001,light_jets->at(i)->eta(),light_jets->at(i)->phi(),light_jets->at(i)->m()*0.001);
      
      for(int j=i+1; j<light_jets->size(); j++){

	TLorentzVector *w_j2 = new TLorentzVector();
	w_j2->SetPtEtaPhiM(light_jets->at(j)->pt()*0.001,light_jets->at(j)->eta(),light_jets->at(j)->phi(),light_jets->at(j)->m()*0.001);
	
	TLorentzVector w = (*w_j1)+(*w_j2);
	mw = w.M();
   
	Xwt = TMath::Sqrt(TMath::Power((mw-80)/(0.1*mw),2)+TMath::Power((mt-173)/(0.1*mt),2));
	
	if(Xwt<Xwt_min){
	    
	  Xwt_min = Xwt;

	}

      }
    }

    Xwt = Xwt_min;

  }

  return Xwt;

}

StatusCode MyxAODAnalysisBTagging :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // Close output file 
  //TFile* ofile = wk()->getOutputFile( m_outputStreamName );
  //ANA_CHECK( evtStore()->event()->finishWritingTo( ofile ) );

  delete m_jetPt;
  delete m_jetEta;
  delete m_jetPhi;

  delete m_jetPt_FTK;
  delete m_jetEta_FTK;
  delete m_jetPhi_FTK;

  /*delete &m_h1Pt;
  delete &m_h2Pt;
  delete &m_h1M;
  delete &m_h2M;
  */
  return StatusCode::SUCCESS;
}
