#ifndef MyAnalysisBTagging_MyxAODAnalysisBTagging_H
#define MyAnalysisBTagging_MyxAODAnalysisBTagging_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <xAODBTaggingEfficiency/BTaggingSelectionTool.h>
#include <QuickAna/IQuickAna.h>
#include <JetInterface/IJetSelector.h>
//#include <JetSelectorTools/JetCleaningTool.h>
#include "JetCalibTools/IJetCalibrationTool.h"

#include "UpgradePerformanceFunctions/UpgradePerformanceFunctions.h"

#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>

#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"

namespace InDet {
  class IInDetTrackSelectionTool;
}

class MyxAODAnalysisBTagging : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysisBTagging (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  

 private:

  std::string m_outputStreamName;
  
  asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_ts;
  asg::AnaToolHandle<IBTaggingSelectionTool> m_btag;

  asg::AnaToolHandle<ana::IQuickAna> m_quickAna;

  asg::AnaToolHandle<IJetSelector> m_jetCleaning;  

  asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibration;

  asg::AnaToolHandle<IBTaggingEfficiencyTool> m_btageff;


  std::unique_ptr<Upgrade::UpgradePerformanceFunctions> m_upgrade;

  bool dRpass(std::pair<const xAOD::Jet*,const xAOD::Jet*>,  std::pair<const xAOD::Jet*,const xAOD::Jet*>);

  double computeDhh(std::pair<TLorentzVector,TLorentzVector>);

  double computeXwt(const xAOD::Jet*, const xAOD::JetContainer*);

  double getFTK(double, double, char);

  TGraph *gr;
  TGraph *gr_forward;

  const char* jet_collection;

  bool pass_HLT_b225_L1_j100(int,const xAOD::JetContainer*,const xAOD::EventInfo*);
  bool pass_HLT_2b35_2j35_L1_4j15(int,const xAOD::JetContainer*,const xAOD::EventInfo*);
  bool pass_HLT_2b55_j100_L1_j75_3j20(int,const xAOD::JetContainer*,const xAOD::EventInfo*);
  
  StatusCode offline_analysis(int,const xAOD::JetContainer*, const xAOD::JetContainer*);

  static bool sort_func(const xAOD::Jet*,const xAOD::Jet*);

  bool doBTagging(int,const xAOD::Jet*,const xAOD::EventInfo*);

  std::string cleanLoc(const std::string&);

  // Tree variables
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number
  // Jets pt
  int m_jetN = -999.;
  std::vector<float> *m_jetPt = nullptr;
  std::vector<float> *m_jetEta = nullptr;
  std::vector<float> *m_jetPhi = nullptr;
  // h1, h2 properties
  float m_h1Pt = -999.;
  float m_h2Pt = -999.;
  float m_h1Eta = -999.;
  float m_h2Eta = -999.;
  float m_h1Phi = -999.;
  float m_h2Phi = -999.;
  float m_h1M = -999.;
  float m_h2M = -999.;

  // For FTK
  // Jets pt
  int m_jetN_FTK = -999.;
  std::vector<float> *m_jetPt_FTK = nullptr;
  std::vector<float> *m_jetEta_FTK = nullptr;
  std::vector<float> *m_jetPhi_FTK = nullptr;
  // h1, h2 properties
  float m_h1Pt_FTK = -999.;
  float m_h2Pt_FTK = -999.;
  float m_h1Eta_FTK = -999.;
  float m_h2Eta_FTK = -999.;
  float m_h1Phi_FTK = -999.;
  float m_h2Phi_FTK = -999.;
  float m_h1M_FTK = -999.;
  float m_h2M_FTK = -999.;
  

};

#endif
