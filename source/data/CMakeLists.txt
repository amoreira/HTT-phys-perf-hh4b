# Declare the name of this package:
atlas_subdir( data )

# Install data files from the package:
atlas_install_data( share/*.csv )