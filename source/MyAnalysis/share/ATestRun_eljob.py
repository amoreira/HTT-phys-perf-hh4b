#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = '/eos/user/a/amoreira/mc15_14TeV.147915.Pythia8_AU2CT10_jetjet_JZ5W.recon.AOD.e1996_s3185_s3186_r9871/'
#inputFilePattern = 'AOD*'
#inputFilePath = os.getenv('ENV_VARIABLE')
#inputFilePath = '/afs/cern.ch/work/a/amoreira/mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871/'
#inputFilePattern = 'AOD*'
#inputFilePath = '/afs/cern.ch/user/a/amoreira/mc15_14TeV.147916.Pythia8_AU2CT10_jetjet_JZ6W.recon.AOD.e1996_s3185_s3186_r9871'
#inputFilePattern = 'AOD.13291105*'
#inputFilePath = '/eos/user/a/amoreira/mc15_14TeV.147913.Pythia8_AU2CT10_jetjet_JZ3W.recon.AOD.e2403_s3185_s3186_r9871/'
#inputFilePattern = 'AOD.13291072*'
#inputFilePath = '/eos/user/a/amoreira/mc15_14TeV.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3251_s3252_r10276/'
#inputFilePattern = 'AOD.13269714*'
#inputFilePath = '/eos/user/a/amoreira/mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3251_s3252_r10276/'
#inputFilePattern = 'AOD.13250489*'

#ROOT.SH.ScanDir().filePattern( inputFilePattern ).scan( sh, inputFilePath )
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3251_s3252_r10276')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147910.Pythia8_AU2CT10_jetjet_JZ0W.recon.AOD.e2403_s3185_s3186_r9871')
#ROOT.SH.scanRucio(sh,'mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.AOD.e3569_s2576_s2132_r6993_r6282')
#ROOT.SH.scanRucio(sh,'mc15_14TeV:mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.AOD.e2176_s3185_s3186_r9871')
ROOT.SH.scanRucio(sh,'mc15_14TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e5458_s3388_s3389_r11135')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3180_s3181_r10703')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3180_s3181_r10452')
#ROOT.SH.scanRucio(sh,'mc15_13TeV:mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.AOD.e3569_s2576_s2132_r6993_r6282')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147911.Pythia8_AU2CT10_jetjet_JZ1W.recon.AOD.e2403_s3185_s3186_r9871')

#ROOT.SH.scanRucio(sh,'mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3251_s3252_r10276')

#ROOT.SH.scanRucio(sh,'mc15_14TeV.147915.Pythia8_AU2CT10_jetjet_JZ5W.recon.AOD.e1996_s3185_s3186_r9871')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147913.Pythia8_AU2CT10_jetjet_JZ3W.recon.AOD.e2403_s3185_s3186_r9871')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.147912.Pythia8_AU2CT10_jetjet_JZ2W.recon.AOD.e2403_s3142_s3143_r9589')
#ROOT.SH.scanRucio(sh,'mc15_14TeV.301493.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M800.recon.AOD.e6011_s3185_s3186_r9871')
#ROOT.SH.scanRucio(sh,'user.amoreira:mydatasetJZ5')
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100 )

# TTreeCache
job.options().setDouble(job.optCacheSize, 10*1024*1024)
job.options().setDouble(job.optCacheLearnEntries, 20)

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here
#alg.UseFTKjets = 0
alg.OutputFileName = 'ANALYSIS'

# Add output stream
job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )

# Add our algorithm to the job
job.algsAdd( alg )

# Run the job using the direct driver.
#driver = ROOT.EL.DirectDriver()
#driver = ROOT.EL.LocalDriver()
#driver = ROOT.EL.CondorDriver()
driver = ROOT.EL.PrunDriver()
#job.options().setString (job.optCondorConf, "+JobFlavour = \"longlunch\"")
#driver.shellInit = "export WorkDir_DIR=/afs/cern.ch/work/a/amoreira/HTTPhysPerfAnalysis/build/x86_64-slc6-gcc62-opt && export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh && export X509_USER_PROXY=/afs/cern.ch/user/a/amoreira/x509up_u106856 && source ${WorkDir_DIR}/setup.sh "

driver.options().setString("nc_outputSampleName", "user.amoreira.ttbar_hdamp258p75_nonallhad_presel_grid_040719")
driver.options().setDouble(job.optGridMergeOutput, 1)
job.options().setString (job.optSubmitFlags, "-v")
driver.submitOnly( job, options.submission_dir )
