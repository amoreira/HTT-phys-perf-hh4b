#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
// xAOD core
#include <xAODCore/AuxContainerBase.h>
// xAOD base 
#include <xAODBase/IParticle.h>
#include <xAODBase/IParticleContainer.h>
// EventInfo
#include <xAODEventInfo/EventInfo.h>
// Tracking
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/TrackParticle.h>
#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <xAODTracking/Vertex.h>
// Btagging
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <xAODBTagging/BTagging.h>
#include <xAODBTaggingEfficiency/BTaggingSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"

//#include <xAODBTagging/BTagInfo.h>
// Jets
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
// Jet cleaning tool
#include <JetInterface/IJetSelector.h>
// Jet calibration
#include <JetCalibTools/IJetCalibrationTool.h>
// Truth
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticle.h>
// QuickAna
#include <QuickAna/IQuickAna.h>
// ROOT includes
#include <TH1F.h>
#include <TH2F.h>
#include <TMath.h>
#include <TRandom3.h>

#include <AthLinks/ElementLink.h>

// Upgrade functions
#include "UpgradePerformanceFunctions/UpgradePerformanceFunctions.h"

#include <unistd.h>
#include <stdio.h>
#include <limits.h>

#include <PathResolver/PathResolver.h>

#include <AsgTools/AnaToolHandle.h>

#include "EventLoop/Worker.h"

using namespace std;
using namespace Upgrade;

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
    m_quickAna ("ana::QuickAna/quickAna",this)

{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  // Declare tools

  declareProperty("OutputFileName",m_outputStreamName = "ANALYSIS");

}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO("in initialize");
  
 // Book histos for jets
  ANA_CHECK(book(TH1F("hist_antikt4EMPFjet_pt","hist_antikt4EMPFjet_pt",500,0,50)));
  ANA_CHECK(book(TH1F("hist_antikt4EMPFjet_eta","hist_antikt4EMPFjet_eta",100,-5,5)));
  ANA_CHECK(book(TH1F("hist_antikt4EMPFjet_phi","hist_antikt4EMPFjet_phi",100,-3.2,3.2)));
  
  // Book histos for jets with pt>20 GeV and |eta|<4.5 
  ANA_CHECK(book(TH1F("hist_antikt4EMPFjet_pt_ptcut20_etacut45","hist_antikt4EMPFjet_pt_ptcut20_etacut45",500,0,50)));
  ANA_CHECK(book(TH1F("hist_antikt4EMPFjet_eta_ptcut20_etacut45","hist_antikt4EMPFjet_eta_ptcut20_etacut45",100,-5,5)));
  ANA_CHECK(book(TH1F("hist_antikt4EMPFjet_N_ptcut20_etacut45","hist_antikt4EMPFjet_N_ptcut20_etacut45",10,0,10)));
  
  // Trigger plots
  ANA_CHECK(book(TH1F("hist_trig_HLT_b225_L1_j100","HLT_b225_L1_j100",4,-.5,3.5)));
  ANA_CHECK(book(TH1F("hist_trig_HLT_2b35_2j35_L1_4j15","HLT_2b35_2j35_L1_4j15",4,-.5,3.5)));
  ANA_CHECK(book(TH1F("hist_trig_HLT_2b55_j100_L1_j75_3j20","HLT_2b55_j100_L1_j75_3j20",4,-.5,3.5)));

  // Setting up output stream
  TFile* ofile = wk()->getOutputFile( m_outputStreamName );
  if( ! ofile ) {
    // Handle the error...
    return StatusCode::FAILURE;
  }
  ANA_CHECK( evtStore()->event()->writeTo( ofile ) );

  evtStore()->event()->setAuxItemList( "EventInfoAux.","-subEventTime.-subEventLink" );
  //evtStore()->event()->setAuxItemList( "EventInfoAux.", "-" );

  return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute(){

  double jet_pt_min = 20.;
 
  ANA_MSG_INFO("in execute");

  // Retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve(eventInfo,"EventInfo"));

  //eventInfo->subEvents();

  // Print out run and event number from retrieved object
  ANA_MSG_INFO("in execute, runNumber="<<eventInfo->runNumber()<<",eventNumber="<<eventInfo->eventNumber());
 
  // Tell QuickAna to process event
  ANA_CHECK (m_quickAna->process());
 
  //------------------------------------------
  // Retrieve collections
  //------------------------------------------

  // Reco anti-kt R=0.4 jets
  const xAOD::JetContainer *antikt4EMPFjets =  nullptr;
  ANA_CHECK(evtStore()->retrieve(antikt4EMPFjets,"AntiKt4EMPFlowJets"));

  //-----------------------------------------------
  // Loop over collections and do basic selection
  //-----------------------------------------------
  // All jets with pt>15 and qta<4,5
  auto selJets_all = std::make_unique<xAOD::JetContainer>();
  auto selJetsAux_all = std::make_unique<xAOD::AuxContainerBase>();
  selJets_all->setStore (selJetsAux_all.get()); //< Connect the two

  int count_jets = 0;

  // Loop over reco jets
  for(const xAOD::Jet *antikt4EMPFjet : *antikt4EMPFjets){

    // Clean bad jets (loose selection)
   
    hist("hist_antikt4EMPFjet_pt")->Fill(antikt4EMPFjet->pt()*0.001);
    hist("hist_antikt4EMPFjet_eta")->Fill(antikt4EMPFjet->eta());
    hist("hist_antikt4EMPFjet_phi")->Fill(antikt4EMPFjet->phi());

    // Apply basic kinematic cuts
    if(antikt4EMPFjet->pt()*0.001<jet_pt_min || TMath::Abs(antikt4EMPFjet->eta())>4.5) continue;

    xAOD::Jet* selJet_all = new xAOD::Jet();
    selJets_all->push_back(selJet_all);
    *selJet_all = *antikt4EMPFjet;
    
    count_jets+=1;
    hist("hist_antikt4EMPFjet_pt_ptcut20_etacut45")->Fill(antikt4EMPFjet->pt()*0.001);
    hist("hist_antikt4EMPFjet_eta_ptcut20_etacut45")->Fill(antikt4EMPFjet->eta());

  } // End of loop over AntiKt4EMPFlowJets  

  hist("hist_antikt4EMPFjet_N_ptcut20_etacut45")->Fill(count_jets);

  ANA_CHECK (evtStore()->record (selJets_all.release(), "SelJetsAll"));
  ANA_CHECK (evtStore()->record (selJetsAux_all.release(), "SelJetsAllAux."));
  
  //------------------------------------------------
  // Retrieve selected collections
  //------------------------------------------------
   
  // Selected jets (jet cleaning, pt>20, eta<4.5 and truth level b tag using upgrade functions)
  // All jets 
  const xAOD::JetContainer *sel_jet_all = nullptr;
  ANA_CHECK(evtStore()->retrieve(sel_jet_all,"SelJetsAll"));

  // Trigger chains
  // Offline 
  hist("hist_trig_HLT_b225_L1_j100")->Fill(0);
  hist("hist_trig_HLT_2b35_2j35_L1_4j15")->Fill(0);
  hist("hist_trig_HLT_2b55_j100_L1_j75_3j20")->Fill(0);
  
  // Trigger chains with offline b-tagging
  ANA_MSG_INFO("Offline b-tagging");
  bool pass_chain1 = pass_HLT_b225_L1_j100(sel_jet_all,eventInfo);
  bool pass_chain2 = pass_HLT_2b35_2j35_L1_4j15(sel_jet_all,eventInfo);
  bool pass_chain3 = pass_HLT_2b55_j100_L1_j75_3j20(sel_jet_all,eventInfo);

  if(pass_chain1 || pass_chain2 || pass_chain3){
    ANA_CHECK(evtStore()->event()->copy("AntiKt4EMPFlowJets"));
    ANA_CHECK(evtStore()->event()->copy("EventInfo"));
    evtStore()->event()->fill();
  }
  return StatusCode::SUCCESS;

}

bool MyxAODAnalysis::pass_HLT_b225_L1_j100(const xAOD::JetContainer *jet_col, const xAOD::EventInfo *EventInfo){

  string hist_name = "hist_trig_HLT_b225_L1_j100";

  bool pass = false;

  int countL1_j100 = 0;
  int countHLT_j225 = 0;

  for(const xAOD::Jet *jet : *jet_col){

    if(jet->pt()*0.001>100. && TMath::Abs(jet->eta())<2.5) countL1_j100 += 1;

  }

  if(countL1_j100<1){
    EventInfo->auxdecor<int>("pass_HLT_b225_L1_j100")=0;
    return pass;
  } else{

    hist(hist_name)->Fill(1);
    for(const xAOD::Jet *jet : *jet_col){

      if(jet->pt()*0.001>225.) countHLT_j225 += 1;

    }
    
    if(countHLT_j225<1){
      EventInfo->auxdecor<int>("pass_HLT_b225_L1_j100")=0;
      return pass;
    }else{
      hist(hist_name)->Fill(2);
      EventInfo->auxdecor<int>("pass_HLT_b225_L1_j100")=1;
      pass = true;
    }  
  }

  return pass;
}

bool MyxAODAnalysis::pass_HLT_2b55_j100_L1_j75_3j20(const xAOD::JetContainer *jet_col, const xAOD::EventInfo *EventInfo){

  string hist_name = "hist_trig_HLT_2b55_j100_L1_j75_3j20";

  bool pass = false;

  int countL1_j75 = 0;
  int countL1_3j20 = 0;

  int countHLT_3j55 = 0;
  int countHLT_j100 = 0;

  for(const xAOD::Jet * jet : *jet_col){
    
    if(jet->pt()*0.001>75.) countL1_j75 += 1;

    if(jet->pt()*0.001>20.) countL1_3j20 += 1;

  }

  if(countL1_j75<1 || countL1_3j20<3){
    EventInfo->auxdecor<int>("pass_HLT_2b55_j100_L1_j75_3j20")=0;
    return pass;
  }
  else{

    hist(hist_name)->Fill(1);

    for(const xAOD::Jet *jet : *jet_col){

      if(jet->pt()*0.001>55.) countHLT_3j55 += 1;

      if(jet->pt()*0.001>100.) countHLT_j100 += 1;

    }

    if(countHLT_3j55<3 || countHLT_j100<1){
      EventInfo->auxdecor<int>("pass_HLT_2b55_j100_L1_j75_3j20")=0;
      return pass;
    }
    else{
      hist(hist_name)->Fill(2);
      EventInfo->auxdecor<int>("pass_HLT_2b55_j100_L1_j75_3j20")=1;
      pass = true;
    }
  }
  
  return pass;

}

bool MyxAODAnalysis::pass_HLT_2b35_2j35_L1_4j15(const xAOD::JetContainer *jet_col, const xAOD::EventInfo *EventInfo){

  string hist_name = "hist_trig_HLT_2b35_2j35_L1_4j15";

  bool pass = false;

  int countL1_4j15 = 0;
  int countHLT_4j35 = 0;

  for(const xAOD::Jet *jet : *jet_col){
    
    if(jet->pt()*0.001>15. && TMath::Abs(jet->eta())<2.5) countL1_4j15 += 1;

  }

  if(countL1_4j15<4){
    EventInfo->auxdecor<int>("pass_HLT_2b35_2j35_L1_4j15")=0;
    return pass;
  }else{

    hist(hist_name)->Fill(1);

    for(const xAOD::Jet *jet : *jet_col){

      if(jet->pt()*0.001>35.) countHLT_4j35 += 1;

    }

    if(countHLT_4j35<4){
      EventInfo->auxdecor<int>("pass_HLT_2b35_2j35_L1_4j15")=0;
      return pass;
    }else{
      
      hist(hist_name)->Fill(2);
      EventInfo->auxdecor<int>("pass_HLT_2b35_2j35_L1_4j15")=1;
      pass = true;
    }
  }
  
  return pass;
}

StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // Close output file 
  TFile* ofile = wk()->getOutputFile( m_outputStreamName );
  ANA_CHECK( evtStore()->event()->finishWritingTo( ofile ) );

  return StatusCode::SUCCESS;
}
