#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <InDetTrackSelectionTool/IInDetTrackSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <xAODBTaggingEfficiency/BTaggingSelectionTool.h>
#include <QuickAna/IQuickAna.h>
#include <JetInterface/IJetSelector.h>
//#include <JetSelectorTools/JetCleaningTool.h>
#include "JetCalibTools/IJetCalibrationTool.h"

#include "UpgradePerformanceFunctions/UpgradePerformanceFunctions.h"

#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>

#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"

namespace InDet {
  class IInDetTrackSelectionTool;
}

class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  

 private:

  std::string m_outputStreamName;
  
  asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_ts;
  asg::AnaToolHandle<IBTaggingSelectionTool> m_btag;

  asg::AnaToolHandle<ana::IQuickAna> m_quickAna;

  asg::AnaToolHandle<IJetSelector> m_jetCleaning;  

  asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibration;

  asg::AnaToolHandle<IBTaggingEfficiencyTool> m_btageff;


  std::unique_ptr<Upgrade::UpgradePerformanceFunctions> m_upgrade;

  bool dRpass(std::pair<const xAOD::Jet*,const xAOD::Jet*>,  std::pair<const xAOD::Jet*,const xAOD::Jet*>);

  double computeDhh(std::pair<TLorentzVector,TLorentzVector>);

  double computeXwt(const xAOD::Jet*, const xAOD::JetContainer*);

  double getFTK(double, double, char);

  TGraph *gr;
  TGraph *gr_forward;

  const char* jet_collection;

  bool pass_HLT_b225_L1_j100(const xAOD::JetContainer*,const xAOD::EventInfo*);
  bool pass_HLT_2b35_2j35_L1_4j15(const xAOD::JetContainer*,const xAOD::EventInfo*);
  bool pass_HLT_2b55_j100_L1_j75_3j20(const xAOD::JetContainer*,const xAOD::EventInfo*);
  
  StatusCode offline_analysis(int,const xAOD::JetContainer*, const xAOD::JetContainer*);

  static bool sort_func(const xAOD::Jet*,const xAOD::Jet*);

  std::string cleanLoc(const std::string&);

};

#endif
